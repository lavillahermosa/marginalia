import router from './src/router.js';
import viewer from './src/viewer.js';

const routes = router();

const view = viewer({
    onLoadListing:async(type, parent = null) => {
        const response = await routes.loadListing(type, parent);
        view.updateListing(type, response);

    },
    
    onSaveListingRow:async(type, data, id = null, parent=null) => {
        const response = await routes.saveListingRow(type, data, id, parent);
        view.updateListing(type, response);
    },

    onCopyListingRow:async(type, data, id, parent = null) => {
        const response = await routes.copyListingRow(type, data, id, parent);
        view.updateListing(type, response);
    },
    
    onSetListingRow:async(type, data, id, parent = null) => {
        const response = await routes.setListingRow(type, data, id, parent);
    },

    onDeleteListingRow:async(type, id) => {
        const response = await routes.deleteListingRow(type, id);

    },

    onSaveListingOrder:async(type, $elements, parent=null) => {
        const response = await routes.saveListingOrder(type, $elements, parent);
    },

    onSetPassword:async(password) => {
        const response = await routes.setPassword(password);
        
    }

    


});

view.init();
routes.init();