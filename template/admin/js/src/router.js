const router = () => {

    const init = () => {
        console.log('router init');
    };

    const loadListing = async(type, parent) => {
        return request(window.location.href, {'action':'loadListing', 'type':type, 'parent':parent});
    };

    const saveListingRow = async(type, data, id, parent) => {
        return request(window.location.href, {'action':'saveListingRow', 'type':type, 'data':data, 'id':id, 'parent':parent});
    };

    const saveListingOrder = async(type, $elements, parent) => {
        const data = [];
        for(let [n, $el] of [...$elements].entries()){
            data.push({'id':$el.getAttribute('data-id'), 'n':n+1});


        }
        return request(window.location.href, {'action':'saveListingOrder', 'type':type, 'data':data, 'parent':parent});
    };

    const copyListingRow = async(type, data, id, parent) => {
        return request(window.location.href, {'action':'copyListingRow', 'type':type, 'data':data, 'id':id, 'parent':parent});
    };

    const setListingRow = async(type, data, id, parent) => {
        return request(window.location.href, {'action':'setListingRow', 'type':type, 'data':data, 'id':id, 'parent':parent});
    };

    const deleteListingRow = async(type, id) => {
        return request(window.location.href, {'action':'deleteListingRow', 'type':type, 'id':id});
    };

    const setPassword = async(password) => {
        return request(window.location.href, {'action':'setPassword', 'data':password});
    };

    const request = async(requestUrl, postData = {}, loader = false) => {
        // -[ ] adapt loader
        if(loader){
            var $loader = document.createElement('div');
            $loader.setAttribute('id', 'loader');
            let $itemPanel = document.querySelector('.item-panel');
            if ($itemPanel!=null)
                $itemPanel.appendChild($loader);
        }
    
        let response = await fetch(requestUrl, {
          method: 'POST', // *GET, POST, PUT, DELETE, etc.
          mode: 'cors', // no-cors, *cors, same-origin
          cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
          credentials: 'same-origin', // include, *same-origin, omit
          headers: {
            'X-Requested-With': 'XMLHttpRequest',
            'Content-Type': 'application/json'
            // 'Content-Type': 'application/x-www-form-urlencoded',
          },
          redirect: 'follow', // manual, *follow, error
          referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
          body: JSON.stringify(postData) // body data type must match "Content-Type" header*/
        });
    
        

        return response.json(); // parses JSON response into native JavaScript objects
    }

    return({init, loadListing, saveListingRow, copyListingRow, deleteListingRow, saveListingOrder, setListingRow, setPassword});
};

export default router;