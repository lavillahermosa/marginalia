const viewer = ({
    onLoadListing=null, 
    onSaveListingRow=null, 
    onSaveListingOrder=null,
    onCopyListingRow=null,
    onDeleteListingRow = null,
    onSetListingRow=null,
    onSetPassword=null
    }={}) => {


    //the listings
    const $listings = {
        'chapters':document.querySelector('.listing-chapters'),
        'interfaces':document.querySelector('.listing-interfaces'),
        'pads':document.querySelector('.listing-pads')
    };



    //lock interactions if something is already going on
    let actionInProgress = false;



    const init = () => {
        console.log('init viewer');

        onLoadListing('chapters');
        
        initActions(document);


    }

    const initActions = ($element) => {
        let $buttons;
        if($element.classList && $element.classList.contains('action'))
            $buttons = [$element];
        else
            $buttons = $element.querySelectorAll('.action');
        for(let $button of $buttons){
            $button.addEventListener('click', (e) => {
                if(actionInProgress){
                    return;
                }

                e.stopPropagation();
                
                const action = $button.getAttribute('data-action');
                const type = $button.getAttribute('data-type');
                const id = $button.getAttribute('data-id');
                const parent = $button.getAttribute('data-parent');
                triggerAction(action, type, id, parent, $button);
            });
        }
    };

    const triggerAction = (action, type, id, parent, $button) => {
        
        switch(action){
            
            case 'add':
                console.log('add row');
                $button.classList.add('active');
               
                actionInProgress = true;
                addListingRow(type, parent, () => {
                    actionInProgress = false;
                    $button.classList.remove('active');
                });
                break;
            case 'move':
                $button.classList.add('active');
                actionInProgress = true;
                console.log('lets move');
                startMove(type, id, parent, () => {
                    actionInProgress = false;
                    $button.classList.remove('active');
                });
                break;
            case 'rename':
                actionInProgress = true;
                console.log('rename');
                renameListingRow(type, id, parent, () =>{
                    actionInProgress = false;
                });
                break;
            case 'select':
                
                if($button.classList.contains('active')){
                    console.log('unselect');
                    $button.classList.remove('active');
                    unselectListingRow(type, id);
                    return;
                }
                console.log('select');
                $button.classList.add('active');
                selectListingRow(type, id, parent);
                break;
            case 'copy':
                console.log('copy');
                copyListingRow(type, id, parent, () => {
                    actionInProgress = false;
                });
                break;

            case 'delete':
                console.log('delete');
                deleteListingRow(type, id, () => {
                    actionInProgress = false;
                });
                break;
            case 'lock':
                console.log('lock/unlock');
                actionInProgress = true;
                if($button.classList.contains('locked')){
                    toggleLockListingRow(type, id, parent, false, () => {
                        $button.classList.remove('locked');
                        actionInProgress = false;
                    });
                }else{
                    toggleLockListingRow(type, id, parent, true, () => {
                        $button.classList.add('locked');
                        actionInProgress = false;
                    });
                }
                break;
            case 'passwd':
                console.log('set new password for user');
                actionInProgress = true;
                setPassword(() => {
                    actionInProgress = false;
                });
                break;
            case 'write':
            case 'design':
                console.log('view writing/design interface');
                console.log(window.location);
                const itfURL = $button.getAttribute('data-url');

                window.open(window.location.href.replace('ctrl', `${action}/`) + itfURL, '_blank');
                break;

            default:
                console.log('unknown action', action);
        }


    };

    //ON SELECT, SHOW NEXT LISTING
    const selectListingRow = async(type, id) => {


        const $elem = $listings[type].querySelector(`li[data-id="${id}"]`);
       
        //is there an active elem already?
        const $activeElem = $listings[type].querySelector('li.active');
        if($activeElem != null){
            unselectListingRow(type, $activeElem.getAttribute('data-id'));
        }
        
        $elem.classList.add('active');

        //show next section (which contains the children of the selected elem) 
        const $nextSection = $listings[type].parentElement.nextElementSibling;
        $nextSection.classList.remove('hidden');

        //update action on add button for nextSection
        $nextSection.querySelector('.button.add').setAttribute('data-parent', id);

        await onLoadListing($nextSection.getAttribute('data-type'), id);
        

    };

    const unselectListingRow = (type, id) => {
        const $elem = $listings[type].querySelector(`li[data-id="${id}"]`);
        $elem.classList.remove('active');
        const $activeBtns = $elem.querySelectorAll('.active');
        
        [...$activeBtns].map($activeBtn => $activeBtn.classList.remove('active'));

        //hide all the next sections
        const $currentSection = $listings[type].parentElement;
        let $subSection = $currentSection;
        while($subSection = $subSection.nextElementSibling){
            $subSection.classList.add('hidden');
            $subSection.querySelector('.listing').innerHTML = '';
            
        }

    }

    
    const addListingRow = (type, parent, onFinish) => {
        console.log(type);
        //if row selected, unselect
        const $activeElem = $listings[type].querySelector('li.active');
        if($activeElem != null){
            unselectListingRow(type, $activeElem.getAttribute('data-id'));
        }

        const $li = document.createElement('li');

        $li.classList.add('active');
        const $input = document.createElement('input');
        $input.setAttribute('type', 'text');
        $li.append($input);
        
        $input.addEventListener('focusout', (e) => {
            //save to the dbdbdbdb
            if(onSaveListingRow !== null){
                if($input.value != ''){
                    onSaveListingRow(type, {'name':$input.value}, null, parent);
                }else{
                    $li.parentNode.removeChild($li);
                }
                onFinish();
            }
        });
        $listings[type].prepend($li);
        $input.focus();
    };

    const copyListingRow = (type, id, parent, onFinish) => {
        const $oldLi = $listings[type].querySelector(`li[data-id="${id}"]`);
        const oldName = $oldLi.innerText;
        let newName = oldName + ' copy';
        newName = prompt('Please enter the name of the copy', newName);

        if(newName == ''){
            onFinish();
            return;
        }

        if(onCopyListingRow != null){
            onCopyListingRow(type, {'name':newName}, id, parent);
            onFinish();
        }

    };

    const deleteListingRow = (type, id, onFinish) => {

        const actionValidated = confirm("are you sure you want to delete this element?");

        if(!actionValidated){
            onFinish();
            return;
        }

        const $li = $listings[type].querySelector(`li[data-id="${id}"]`);
        console.log($li);
        unselectListingRow(type, id);
        
        $li.parentElement.removeChild($li);
        
        if(onDeleteListingRow != null){
            onDeleteListingRow(type, id);
        }
        onFinish();

        

    };

    const renameListingRow = (type, id, parent, onFinish)  => {
        const $li = $listings[type].querySelector(`li[data-id="${id}"]`);
        $li.classList.add('active');
        const oldContent = $li.innerHTML;
        const currentName = $li.innerText;
        $li.innerHTML = '';
        const $input = document.createElement('input');
        $input.setAttribute('type', 'text');
        $input.value = currentName;
        $li.append($input);
        $input.addEventListener('focusout', (e) => {
            //save to the dbdbdbdb
            if(onSaveListingRow !== null){
                if($input.value != '' && $input.value != currentName){
                    onSaveListingRow(type, {'name':$input.value}, id, parent);
                }else{
                    $li.innerHTML = oldContent;
                    initActions($li);
                }
                $li.classList.remove('active');
                onFinish();
            }
        });
        $input.focus();


    };

    const toggleLockListingRow = (type, id, parent, value, onFinish) => {
        onSetListingRow(type, {'lock':value}, id, parent);
        if(onFinish != false){
            onFinish();
        }
    };


    const updateListing = (type, data) => {

        $listings[type].innerHTML = data['response'];
        initActions($listings[type]);

    };

   
    let mousePosInit, nInit;

    const moveListingRow = (steps, $elem, type) => {
        //console.log(steps);
        const $elems = $elem.parentElement.children;
        
        const n = Math.min(Math.max(nInit + steps, 0), $elems.length - 1);

        //which one is at target n?
        const $elem2 = [...$elems][n];

        if(steps > 0){
            $elem.parentElement.insertBefore($elem, $elem2.nextSibling);
        }else{
            $elem.parentElement.insertBefore($elem, $elem2);
        }
        nInit = n;


    };

    const startMove = (type, id, parent, onFinish) =>{
        mousePosInit = null;
        const $elem = $listings[type].querySelector(`li[data-id="${id}"]`);
        $elem.classList.add('active');
        nInit = [...$elem.parentElement.children].indexOf($elem);

        const moveFn = move.bind(null, $elem, type);
        document.addEventListener('mousemove', moveFn);
        document.addEventListener('click', (e) => {
            $elem.classList.remove('active');
            document.removeEventListener('mousemove', moveFn);
            if(onSaveListingOrder !== null){
                onSaveListingOrder(type, $listings[type].children, parent);
            }
            onFinish();
        }, {'once': true});
    };


    const move = ($elem, type, e) => {

    
        if(mousePosInit == null){
            mousePosInit = $elem.getBoundingClientRect().top;
            return;
        }
        const dist = e.clientY - mousePosInit;
        const steps = Math.floor(dist / ($elem.offsetHeight));
        if(steps !== 0){
            mousePosInit = null;
            moveListingRow(steps, $elem, type);
        }
        
      
    
    };

    const setPassword = async(onFinish) => {
        const newPassword = prompt("please enter your new password");
        //TODO check for validity
        if(onSetPassword != null){
            await onSetPassword(newPassword);
            window.location.href = window.location.href;
        }
        onFinish();

    };

    return ({init, updateListing});
};
export default viewer;