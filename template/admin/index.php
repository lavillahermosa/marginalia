<?php if(!isset($marginalia)){//no direct access
    header('Location: ../');
    exit();
}
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Marginalia</title>
        <link rel="stylesheet" href="<?= $config->rootHttp ?>/template/admin/css/admin.css">
        <script type="module" src="<?= $config->rootHttp ?>/template/admin/js/admin.js" defer></script>
    </head>
    <body>
    <header>
            <h1><?= $config->projectName ?></h1>
    </header>
        <main>
           
            <section class="ui">
                
                <section class="chapters" data-type="chapters">
                    <header>
                        <h2>chapters</h2>
                        <div class="tools"><span class="button action add" data-action="add" data-type="chapters"></span></div>
                    </header>
                    <ul class="listing listing-chapters"></ul>
                </section>
                <section class="interfaces hidden" data-type="interfaces">
                    <header>
                        <h2>interfaces</h2>
                        <div class="tools"><span class="button action add" data-action="add" data-type="interfaces"></span></div>
                    </header>
                    <ul class="listing listing-interfaces"></ul>
                </section>
                
                <section class="pads hidden" data-type="pads">
                    <header>
                        <h2>pads</h2>
                        <div class="tools"><span class="button action add" data-action="add" data-type="pads"></span></div>
                    </header>
                    <ul class="listing listing-pads"></ul>
                </section>
                
            </section>
        </main>
        <footer>
            <div class="tools"><span class="button action passwd" data-action="passwd"></div>
        </footer>
    </body>
</html>