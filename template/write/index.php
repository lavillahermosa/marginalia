<?php

if(!isset($marginalia)){//no direct access 
    header('Location: ../');
    exit();
}


?>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Marginalia - writing</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="<?= $config->rootHttp ?>/template/write/css/interface.css">

<?php
    $q = json_decode($marginalia->query(['action' => 'loadHeader', 'url' => $itfURL], 'write'));
    echo $q->response->content;
?>
        <script type="module" src="<?= $config->rootHttp ?>/template/write/js/interface.js" defer></script>
        <script src="<?= $config->rootHttp ?>/template/write/js/src/ckeditor/ckeditor.js"></script>
    </head>
    <body>
        
    </body>
</html>