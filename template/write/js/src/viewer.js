const viewer = ({
    onLoadInterface=null,
    onAddBlock = null,
    onEditBlock = null,
    onDeleteBlock = null,
    onBlocksSort = null,
    onResizePads = null, 
    onPadSetting = null
    }={}) => {



    //lock interactions if something is already going on
    let actionInProgress = false;

    const itfURL = new URL(window.location).pathname.split('/').filter(Boolean).pop();

    const ckConfig = {
        ui: {poweredBy: {position: 'inside',label: ''}}, 
        toolbar: [ 'heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote', 'imageInsert', 'undo', 'redo'],
    
        htmlSupport:{
            allow: [ { 
                name: 'span',
                classes: ['bracket', 'bracket-open', 'bracket-close', 'selection'],
                attributes:true
            } ]
        },
        image: {
            toolbar: [ 'toggleImageCaption', 'imageTextAlternative']
        },
        simpleUpload: {
            uploadUrl: window.location.href,
            headers: {
                'uploadType':'image'
            }
        }
    };


    let currentSelection;


    const init = async() => {
        console.log('init viewer');

        await onLoadInterface(itfURL);
        initButtons(document);

        initEvents();
        
        

    }

    //global events. Useful for general interactions.
    const initEvents = () => {


        document.addEventListener('mouseup', (e) => {

            //store selection in memory.
            const selection = document.getSelection();
            //console.log(selection);

            if(selection.type == 'None' ||
                (selection.anchorNode.nodeType == Node.ELEMENT_NODE && selection.anchorNode.classList.contains('action'))){
                return;
            }
            if(selection.type == 'Caret'){
                currentSelection = null;   
            }else{
                currentSelection = {
                    'anchorNode':selection.anchorNode,
                    'focusNode':selection.focusNode,
                    'focusOffset':selection.focusOffset,
                    'anchorOffset':selection.anchorOffset
                
                };
            }
            //console.log(currentSelection);
            
        });



    }



    //BUTTONS HANDLING
    const initButtons = ($element) => {

        let $buttons;
        if($element.classList && $element.classList.contains('action'))
            $buttons = [$element];
        else
            $buttons = $element.querySelectorAll('.action');

        for(let $button of $buttons){

            const action = $button.getAttribute('data-action');
            const id = $button.getAttribute('data-id');
            const parent = $button.getAttribute('data-parent');
            const actionType = $button.getAttribute('data-action-type');
            
            
            if(action != null){
                
                $button.addEventListener(actionType, (e) => {
                    if(actionInProgress){
                        return;
                    }

                    e.stopPropagation();
                    
                    triggerAction(action, id, parent, $button);
                });
            }
            
        }
    };

    const triggerAction = (action, id, parent, $elem) => {
        
        switch(action){
            
            case 'add':
                console.log('add block');
                $elem.classList.add('active');
                const n = $elem.getAttribute('data-n');
                actionInProgress = true;
                actionAddBlock(parent, n, (result) => {
                    //if result == false : empty block
                    actionInProgress = false;
                    $elem.classList.remove('active');
                });
                break;
            case 'edit':
                console.log('edit block');
                $elem.classList.add('active');
                actionInProgress = true;
                actionEditBlock(parent, id, () => {
                    actionInProgress = false;
                    $elem.classList.remove('active');
                });
                break;
            case 'highlight':
                console.log('start highlight');

                
                $elem.classList.add('active');
                actionInProgress = true;
                actionHighlight(parent, id, () => {
                    actionInProgress = false;
                    $elem.classList.remove('active');
                    //console.log('onfinish');
                });
                break;

            case 'delete':
                console.log('start delete');
                $elem.classList.add('active');
                actionInProgress = true;
                actionDeleteBlock(parent, id, () => {
                    actionInProgress = false;
                    $elem.classList.remove('active');
                });
                break;
            case 'resize':
                console.log('start resize');
                actionInProgress = true;
                actionResize($elem, () => {
                    actionInProgress = false;

                });

                break;
            case 'settings':
                console.log('start settings');
                
                if($elem.classList.contains('active')){
                    actionHideSettings(parent);
                    $elem.classList.remove('active');
                }else{
                    actionShowSettings(parent);
                    $elem.classList.add('active');
                }
                
                break;
            case 'setFontSize':
                console.log('set font size');
                actionInProgress = true;
                actionSetFontSize(parent, $elem.value, () => {
                    actionInProgress = false;
                });
                break;
            case 'setRefDisplayMode':
                console.log('set ref display mode');
                actionInProgress = true;
                actionSetRefDisplayMode(parent, $elem.value, () => {
                    actionInProgress = false;
                });
                break;
            case 'sort':
                console.log('sort notes');
                actionInProgress = true;
                actionSort(parent, $elem.value, () => {
                    actionInProgress = false;
                });
                break;
            default:
                console.log('unknown action', action);
        }


    };

    /////////////////////////////////////////////////////////////////////////////////
    //ACTIONS
    //action can be triggered by an event (in case of triggerAction) or by another action (in case of chained actions)


    //actionSettings: show and hide settings window for pad
    const actionShowSettings = (parent) => {
        console.log('show settings');
        const $settings = document.querySelector(`section.pad[data-id='${parent}'] div.settings`);
        console.log($settings);
        $settings.classList.add('active');
       
    };
    const actionHideSettings = (parent) => {
        const $settings = document.querySelector(`section.pad[data-id='${parent}'] div.settings`);
        $settings.classList.remove('active');
       
    };


    const actionSetFontSize = async(parent, value, onFinish) => {
        const $padContent = document.querySelector(`section.pad[data-id='${parent}'] .pad-content`);
        $padContent.style.fontSize = `${0.5 + value / 5}rem`;
        if(onPadSetting != null){
            await onPadSetting(itfURL, {'key':'fontsize', 'value':value}, parent);
        }
        
        onFinish();

    };

    const actionSetRefDisplayMode = async(parent, value, onFinish) => {
        const $pad = document.querySelector(`section.pad[data-id='${parent}']`);
        $pad.setAttribute('data-ndisplaymode', value);
        if(onPadSetting != null){
            await onPadSetting(itfURL, {'key':'ndisplaymode', 'value':value}, parent);
        }

        onFinish();
    };

    const actionSort = async(parent, mode, onFinish) => {
        const $otherPads = document.querySelectorAll(`section.pad:not([data-id='${parent}'])`);
        const $pad = document.querySelector(`section.pad[data-id='${parent}']`);
        const $padContent = $pad.querySelector('.pad-content');

        $otherPads.forEach($otherPad => {
            const $closingBrackets = $otherPad.querySelectorAll(`.bracket-close[data-to='${parent}']`);
            $closingBrackets.forEach($closingBracket => {
                 
                const $noteRel = $pad.querySelector(`.relation[data-rel-id='${$closingBracket.getAttribute('data-rel-id')}']`);
                const $note = $noteRel.closest('.note');
                $padContent.appendChild($note);

            });

        });

        const $blocks = $pad.querySelectorAll('.block');
        const data = [];
        $blocks.forEach(($block, i) => {
            $block.setAttribute('data-n', i+1);
            data.push({'id':$block.getAttribute('data-id'), 'n':i+1});
        });

        if(onBlocksSort != null){
            await(onBlocksSort(itfURL, data, parent));
        }
        onFinish();



    };

    //actionEditBlock: transform an existing block into an editable area + save on finish
    const actionEditBlock = (parent, id, onFinish) => {
        const $parentPad = document.querySelector(`section.pad[data-id='${parent}']`);
        const $block = $parentPad.querySelector(`div.block[data-id='${id}']`);
        const $editor = $block.querySelector('.block-content');

        //remove the .highlight-relation spans because it's generated on the fly and not part of actual content.
        
        [...$editor.querySelectorAll('.highlight-relation')].forEach($highlightRelation => {
            $highlightRelation.parentElement.removeChild($highlightRelation);
        });
        //store the ids of relations in the block for future processing
        const relationsInBlock = [];
        [...$editor.querySelectorAll('.bracket-close')].forEach($bracket => {
            relationsInBlock.push({'id':$bracket.getAttribute('data-rel-id'),'to':$bracket.getAttribute('data-to')});
            $bracket.removeAttribute('data-to');
        });

        const oldData = $editor.innerHTML;
        let editor;


        const checkAndSave = async e => {
            e.stopPropagation();
            const $ck = editor.ui.view.element;
            const $ckWrapper = document.querySelector('.ck-body-wrapper');
            console.log(editor.ui);
            console.log(editor.ui);
            if($ck.contains(e.target) || $ckWrapper != null && $ckWrapper.contains(e.target)){
                return;
            }
            
            //click outside the editor = save

            document.removeEventListener('click', checkAndSave);

            let data = editor.getData();
            $block.style.height = $ck.offsetHeight+'px';
            editor.destroy();
            $editor.innerHTML = data;
            if(onEditBlock != null && data != oldData){
                //cleanup selections and relations based on brackets
                cleanUpHighlights($editor);
                relationsInBlock.forEach(relation=>{
                    checkRelation(relation['id'], $editor, document.querySelector(`.pad[data-id='${relation['to']}']`));
                });

                data = $editor.innerHTML;

                await onEditBlock(itfURL, data, parent, id, $block);                  

            }else{
                console.log('same data, do nothing');
            }
            setRelations($parentPad);
            
            onFinish();


        };

        
        ClassicEditor
            .create( $editor, ckConfig)
            .then( newEditor => {
                editor = newEditor;
                editor.focus();
                document.addEventListener('click', checkAndSave);
            })
            .catch( error => {
                console.error( error );
            });
    };

    //actionAddBlock: insert a new block + editable area at position n. 
    //If relation != null, the new block is a note related to an highlight.
    const actionAddBlock = async(parent, n, onFinish, {relation = null} = {}) => {
        const $parentPad = document.querySelector(`section.pad[data-id='${parent}']`);
        const $padContent = $parentPad.querySelector('.pad-content');
        

        const $block = document.createElement('div');

        const $blockContent = document.createElement('div');
        const $editor = document.createElement('div');

        $block.classList.add('block');
        $blockContent.classList.add('block-main', 'new-block');
        $editor.classList.add('block-editor');

        $blockContent.append($editor);
        $block.append($blockContent);


        //check if there is already a block at n pos. if so, we need to insert the new one before it
        const $elemAtN = $padContent.querySelector(`.block[data-n='${n}']`);
        if($elemAtN == null){
            $padContent.append($block);
        }else{
            $padContent.insertBefore($block, $elemAtN);
        }

        let editor;

        const checkAndSave = async e => {
            e.stopPropagation();
            const $ck = editor.ui.view.element;
            if($ck.contains(e.target)){
                return;
            }
            
            //click outside the editor = save

            document.removeEventListener('click', checkAndSave);
            const data = editor.getData();
            $block.classList.add('loader');

            $block.style.height = $ck.offsetHeight+'px';
            editor.destroy();

            if(onAddBlock != null && data!=''){
                //update n
                let $nextElem = $block.nextElementSibling;
                while($nextElem != null){
                    //console.log($nextElem);
                    let currentN = parseInt($nextElem.getAttribute('data-n'));
                    $nextElem.setAttribute('data-n', currentN + 1);
                    let $otherElems = $nextElem.querySelectorAll('[data-n]');
                    [...$otherElems].forEach(($el) => {$el.setAttribute('data-n', currentN+2)});
                    $nextElem = $nextElem.nextElementSibling;
                    
                }
                
                await onAddBlock(itfURL, data, relation, parent, n, $block);
                onFinish(true);
                
            }else{
                $block.parentElement.removeChild($block);
                onFinish(false);
            }

        };

        ClassicEditor
            .create( $editor, ckConfig)
            .then( newEditor => {
                editor = newEditor;

                editor.focus();
                $block.scrollIntoView({'behavior':'smooth', 'block':'center'});
                setTimeout(e => {
                    document.addEventListener('click', checkAndSave);
                }, 1000);

            } )
            .catch( error => {
                console.error( error );
            } );



    }

    //actionDeleteBlock: remove a block (and update highlights and notes)
    const actionDeleteBlock = async(parent, id, onFinish) => {

        const $parentPad = document.querySelector(`section.pad[data-id='${parent}']`);

        const $block = $parentPad.querySelector(`div.block[data-id='${id}']`);
        const $blockContent = $block.querySelector('.block-content');

        const actionValidated = confirm("are you sure you want to delete this element?");

        const n = $block.getAttribute('data-n');

        if(!actionValidated){
            onFinish();
            return;
        }


        //first remove relations starting from this block // it just removes the relations from the notes without changing anything in the db.
        const $padsToUpdate = [];
        [...$blockContent.querySelectorAll('.bracket-close')].forEach($bracket => {
            const $padTo = document.querySelector(`.pad[data-id='${$bracket.getAttribute('data-to')}']`);
            
            removeRelation($bracket.getAttribute('data-rel-id'), 
                $parentPad,
                $padTo);
                
            if(!$padsToUpdate.includes($parentPad)){
                $padsToUpdate.push($parentPad);
            }
        });

        //then if the block is a note:
        if($block.classList.contains('note')){
            
            const $relation = $block.querySelector('.block-relation');
            if($relation != null && $relation.getAttribute('data-from') != null && $relation.getAttribute('data-to') != null){
                //get the source pad where the note is coming from
                const $padFrom = document.querySelector(`.pad[data-id='${$relation.getAttribute('data-from')}']`);
                const $modifiedBlock = removeRelation($relation.getAttribute('data-rel-id'), 
                $padFrom,
                $parentPad);

                const data = getBlockData($modifiedBlock);
                const blockID = $modifiedBlock.getAttribute('data-id');
                const parentID = $padFrom.getAttribute('data-id');
                $padsToUpdate.push($padFrom);
                

                await onEditBlock(itfURL, data, parentID, blockID, $modifiedBlock);

            }
        }

        $block.parentElement.removeChild($block);

        $padsToUpdate.forEach($pad => {

            setRelations($pad);
        });
        
        

        //update N after remove
        let nprime = parseInt(n) + 1;

        for(let nextBlock = $parentPad.querySelector(`.block[data-n='${nprime}']`);
            nextBlock != null; nextBlock = $parentPad.querySelector(`.block[data-n='${nprime}']`)){
                nextBlock.setAttribute('data-n', nprime-1);
                nprime++;
        }

        //remove block from DB
        await onDeleteBlock(itfURL, parent, id);

        onFinish();
        

    };

    //actionHighlight: listen to text selection on block. On selected, highlight + trigger actionAddBlock on selected pad.
    const actionHighlight = (parent, id, onFinish) => {
        const $parentPad = document.querySelector(`section.pad[data-id='${parent}']`);

        const $block = $parentPad.querySelector(`div.block[data-id='${id}']`);
        const $blockContent = $block.querySelector('.block-content');
        let $currentCloseBracket;
        let relID;
        

        const selectionStart = () => {

            console.log('begin selection');
            
        };

        const selectionEnd = (e, sel = null) => {

            const selection = (sel != null)?sel:document.getSelection();
            console.log(selection);
            relID = uniqid();

            const [$openBracket, $closeBracket] = insertBrackets(selection);
            console.log('whats happening');
            highlightBetween($openBracket, $closeBracket, relID);

            $currentCloseBracket = $closeBracket;
            
            //reset selection
            if(selection.removeAllRanges != null){
                selection.removeAllRanges();
            }
            currentSelection = null;


            $blockContent.removeEventListener('selectstart', selectionStart);
            $block.removeEventListener('mouseup', selectionEnd);
            //next phase: select other pad and addBlock
            const $otherPads = document.querySelectorAll(`section.pad:not([data-id='${parent}'])`);

            [...$otherPads].filter($otherPad => $otherPad.getAttribute('data-lock') == 0).forEach($otherPad => {
                $otherPad.classList.add('selectable');
                $otherPad.addEventListener('click', selectToPad);
            });

            $parentPad.addEventListener('mousedown', reset);


        };

        const selectToPad = (e) => {
            const $pads = document.querySelectorAll('section.pad');
            const $toPad = e.target;
            
            [...$pads].forEach($pad => {
                $pad.classList.remove('selectable')
                $pad.removeEventListener('click', selectToPad);
            });
            $parentPad.removeEventListener('mousedown', reset);
            
            addNote($toPad, $currentCloseBracket);
            
        }

        const addNote = ($toPad, $closeBracket) => {
            //first we need to get the n. is there already one note with the same n?
            let n;
            const toPadId = $toPad.getAttribute('data-id');
            $closeBracket.setAttribute('data-to', toPadId);

            
            //we need to get the future N of our current note.
            const noteN = [...$parentPad.querySelectorAll(`.bracket-close[data-to='${toPadId}']`)].indexOf($closeBracket) + 1;

            //we get all the relations with same from
            const [...$noteRelations] = $toPad.querySelectorAll(`.block-relation[data-from='${parent}']`);

            if($noteRelations.length == 0){//first relation of this type. add it to the end
                
                n = $toPad.querySelectorAll('div.block').length + 1;
                
            }else{
                if(noteN <= $noteRelations.length){//there is already a relation of this type at n.
                    n = $noteRelations[noteN - 1].closest('.block').getAttribute('data-n');
                    
                }else{//there is already relations of this type, we insert it at the end
                    n = parseInt($noteRelations[$noteRelations.length - 1].closest('.block').getAttribute('data-n')) + 1;
                    
                }
            }
            
            actionAddBlock($toPad.getAttribute('data-id'), n, async(result) => {
                console.log('add block finished with result', result);
                if(result == false){
                    //the note was not added. just remove the highlight and forget about it.
                    removeHighlight(relID, $blockContent);
                    onFinish();

                }else{
                    const data = getBlockData($block);


                    await onEditBlock(itfURL, data, parent, id, $block);

                    setRelation(relID, $block);
                    
                    
                    onFinish();
                }

            }, {'relation':relID});

        };

        
        const uniqid = (prefix = "", random = false)  => {
            const sec = Date.now() * 1000 + Math.random() * 1000;
            const id = sec.toString(16).replace(/\./g, "").padEnd(14, "0");
            return `${prefix}${id}${random ? `.${Math.trunc(Math.random() * 100000000)}`:""}`;
        };
        
        const insertBrackets = (selection) => {
            //selection works both ways.
            const selectionDirection = selection.anchorNode.compareDocumentPosition(selection.focusNode);
            console.log(selection);
            let openBracket, closeBracket;
            if(selectionDirection == 2){
                openBracket = {'pos' : selection.focusOffset, 'node' : selection.focusNode};
                closeBracket = {'pos' : selection.anchorOffset, 'node' : selection.anchorNode};
            }else if(selectionDirection == 4){
                openBracket = {'pos' : selection.anchorOffset, 'node' : selection.anchorNode};
                closeBracket = {'pos' : selection.focusOffset, 'node' : selection.focusNode};
            }else if(selectionDirection == 0){
                if(selection.anchorOffset > selection.focusOffset){
                    openBracket = {'pos' : selection.focusOffset, 'node' : selection.focusNode};
                    closeBracket = {'pos' : selection.anchorOffset, 'node' : selection.anchorNode};
                }else{
                    openBracket = {'pos' : selection.anchorOffset, 'node' : selection.focusNode};
                    closeBracket = {'pos' : selection.focusOffset, 'node' : selection.anchorNode};
                }
                
            }
            
            let $openBracketContainer = openBracket.node.parentElement;
            let $closeBracketContainer = closeBracket.node.parentElement;

            const $openBracket = document.createElement('span');
            const $closeBracket = document.createElement('span');
            

            [$openBracket, $closeBracket].forEach($bracket => {
                //this doesn't work $bracket.setAttribute('contenteditable', 'false');
                $bracket.setAttribute('data-rel-id',relID);
            });

            $openBracket.innerText = '[';
            $closeBracket.innerText = ']';

            $openBracket.classList.add('bracket-open', 'bracket');
            $closeBracket.classList.add('bracket-close', 'bracket');

            if(openBracket.node == closeBracket.node){
                if(openBracket.node.nodeType == 1){//element
                    $openBracketContainer.insertBefore($openBracket, openBracket.node);
                    $closeBracketContainer.insertBefore($closeBracket, closeBracket.node.nextElementSibling);
                }else{
                    const newNode = openBracket.node.splitText(openBracket.pos);
                    $openBracketContainer.insertBefore($openBracket, newNode);
                    $closeBracketContainer.insertBefore($closeBracket, newNode.splitText(closeBracket.pos - openBracket.pos));
                    
                }
               

            }else{
                if(openBracket.node.nodeType == 1){//element
                    $openBracketContainer.insertBefore($openBracket, openBracket.node);
                    
                }else{
                    $openBracketContainer.insertBefore($openBracket, openBracket.node.splitText(openBracket.pos));
                }
                if(closeBracket.node.nodeType == 1){
                    $closeBracketContainer.insertBefore($closeBracket, closeBracket.node.nextSibling);
                }else{
                    $closeBracketContainer.insertBefore($closeBracket, closeBracket.node.splitText(closeBracket.pos));
                }
            }


            return [$openBracket, $closeBracket];
        };

        const reset = () => {
            $block.removeEventListener('mouseup', selectionEnd);
            $blockContent.removeEventListener('selectstart', selectionStart);

            const $otherPads = document.querySelectorAll(`section.pad:not([data-id='${parent}'])`);
            [...$otherPads].forEach($otherPad => {
                $otherPad.classList.remove('selectable');
                $otherPad.removeEventListener('click', selectToPad);

            });
            $parentPad.removeEventListener('mousedown', reset);
            removeHighlight(relID, $blockContent);

            onFinish();
        };

        /////////////

        //check if there is an already selected text
        if(currentSelection != null && $block.contains(currentSelection.anchorNode)){
            //selection already done. trigger selection end
            selectionEnd(null, currentSelection);
        }
        else{
            $block.addEventListener('mouseup', selectionEnd);
            $blockContent.addEventListener('selectstart', selectionStart);
            document.addEventListener('mousedown', (e) => {
                if(!$blockContent.contains(e.target)){
                    reset();
                }
            }, { once: true });
        }

    }

    const actionResize = ($resizer, onFinish) => {
        const $parentPad = $resizer.closest('.pad');
        const $otherPads = document.querySelectorAll(`.pad[data-id='${$parentPad.getAttribute('data-id')}'] ~ .pad`);

        let xStart = $resizer.getBoundingClientRect().left;

        
        const resizePads = e => {
            const dx = ((e.clientX - xStart) / window.innerWidth) * 100;
            $parentPad.style.flexBasis = +$parentPad.style.flexBasis.replace('%', '') + dx + '%';
            
            [...$otherPads].forEach($pad => {
                $pad.style.flexBasis = +$pad.style.flexBasis.replace('%', '') - (dx / $otherPads.length) + '%';
            });
            xStart = e.clientX;



        }

        document.addEventListener('mousemove', resizePads);

        document.addEventListener('mouseup', (e) => {
            document.removeEventListener('mousemove', resizePads);

            if(onResizePads != null){
                
                const allPads = document.querySelectorAll('.pad');
                const data = [...allPads].map($pad => {
                    return {'id':$pad.getAttribute('data-id'), 'width':+$pad.style.flexBasis.replace('%', '')};
                });
                onResizePads(itfURL, data);
            }
            
            onFinish();
        }, {'once':true});

        

    };

    
    ////////////////END ACTIONS
    //////////////////////////////////////////////////////////////////



    /////////////////////////////////////////////////////////////////
    //////////////UTILS 
    //Utils are functions shared between actions or called directly by the controller.

    const buildInterface = (data) => {
        
        document.body.innerHTML = data['content'];
        setRelations(document);
        

    };

    //insertBlock: insert a new block with data as html in parent at n.
    const insertBlock = (data, parent, n) => {
        const $parentPad = document.querySelector(`section.pad[data-id='${parent}']`);
        const $padContent = $parentPad.querySelector('.pad-content');

        const $template = document.createElement('template');
        data = data.trim(); // never return a text node of whitespace as the result
        $template.innerHTML = data;
        const $block = $template.content.firstChild;
        


        const $nextBlock = $padContent.querySelector(`div.block[data-n='${parseInt(n)+1}']`);
        if($nextBlock != null){
            $padContent.insertBefore($block, $nextBlock);
        }else{
            $padContent.append($block);
        }

        initButtons($block);
        
    }  

    //getBlockData: return the content of block without the highlights
    const getBlockData = ($block) => {
        const $blockContent = $block.querySelector('.block-content');
        const $cloneBlockContent = $blockContent.cloneNode(true);
                [...$cloneBlockContent.querySelectorAll('.highlight-relation')].forEach($highlightRelation => {
                    $highlightRelation.parentElement.removeChild($highlightRelation);
                });
                
        return $cloneBlockContent.innerHTML;
    }


    //cleanUpHighlights: remove single brackets and recompute selections based on brackets
    const cleanUpHighlights = ($blockContent) => {
        //first remove all selections
        [...$blockContent.querySelectorAll('.selection')].forEach($selection => {
            
            const textNode = document.createTextNode($selection.innerText);
            $selection.parentElement.replaceChild(textNode, $selection);
            
        });
        //then loop backwards through brackets. foreach bracket find the corresponding 
        const $brackets = [...$blockContent.querySelectorAll('.bracket')];
        //console.log($brackets);
        for(let i = $brackets.length - 1; i >= 0; i--){
            const $bracket = $brackets[i];
            const relID = $bracket.getAttribute('data-rel-id');
            let messedUpDir = false;
            if(!$bracket.classList.contains('bracket-close')){//we should always have a closing bracket here
                $bracket.parentElement.removeChild($bracket);
                messedUpDir = true;
            }
            
            let otherBracketFound = false;
            for(let j = i - 1; j >= 0; j--){
                const $otherBracket = $brackets[j];
                //console.log('$otherBracket', $otherBracket);
                if($otherBracket.getAttribute('data-rel-id') == relID){
                    if(messedUpDir || otherBracketFound || $otherBracket.classList.contains('bracket-close')){
                        //if the direction of brackets is messed up or the pair is already formed
                        //or this one is a closing bracket, delete
                        $otherBracket.parentElement.removeChild($otherBracket);
                        
                    }
                    else{
                        console.log('bracket is good');
                        otherBracketFound = true;
                    }
                    $brackets.splice(j, 1);
                    i--;
                }

            }
            if(!messedUpDir && !otherBracketFound){
                $bracket.parentElement.removeChild($bracket);
            }
        }


        [...$blockContent.querySelectorAll('.bracket-close')].forEach($closeBracket => {
            const relID = $closeBracket.getAttribute('data-rel-id');
            const $openBracket = $blockContent.querySelector(`.bracket-open[data-rel-id='${relID}'`);
            highlightBetween($openBracket, $closeBracket, relID);

        });



    };

    function highlightBetween($openBracket, $closeBracket, relID) {
        let pastStartNode = false, reachedEndNode = false;
        
        function getCommonParent(){
            const range = new Range();
            range.setStart($openBracket, 0);
            range.setEnd($closeBracket, 0);
            return range.commonAncestorContainer;
        }

        function highlightTextNodes(node) {
            console.log(node);
            if (node == $openBracket) {
                pastStartNode = true;
            } else if (node == $closeBracket) {
                reachedEndNode = true;
            } else if (node.nodeType == 3) {
                if (pastStartNode && !reachedEndNode && !/^\s*$/.test(node.nodeValue)) {
                    if(!node.parentElement.classList.contains('selection')){//if textnode not already in selection
                        const $node = document.createElement('span');
                        $node.classList.add('selection');
                        $node.setAttribute('data-rel-id', relID);
                        $node.appendChild(document.createTextNode(node.nodeValue));
                        node.parentElement.replaceChild($node, node);
                    }
                    
                }
            } else if(node.nodeType == 1 && node.nodeName == 'IMG') {
                //highlight image!
                if(!node.parentElement.classList.contains('selection')){//if node not already in selection
                    const $node = document.createElement('span');
                    const parentElement = node.parentElement;
                    $node.classList.add('selection');
                    $node.setAttribute('data-rel-id', relID);
                    $node.appendChild(node);
                    parentElement.append($node);
                }
            } else {
                for (var i = 0, len = node.childNodes.length; !reachedEndNode && i < len; ++i) {
                    highlightTextNodes(node.childNodes[i]);
                }
            }
        }
        const rootNode = getCommonParent();
        highlightTextNodes(rootNode);
        
    }

    //removeHighlight: remove highlight and brackets with id relID from $parent
    //return the block where the modifications were made.
    const removeHighlight = (relID, $parent = document) => {
        const $relElement = $parent.querySelector(`[data-rel-id='${relID}']`);
        if($relElement == null){
            console.log("no trace of relation in this block. do nothing");
            return;
        }
        const $block = $relElement.closest('.block');

        const $brackets = $parent.querySelectorAll(`.bracket[data-rel-id='${relID}']`);
        [...$brackets].forEach($bracket => $bracket.parentElement.removeChild($bracket));
        
        const highlightedSpans = $parent.querySelectorAll(`span.selection[data-rel-id='${relID}']`);
        [...highlightedSpans].forEach($el => {
            const content = $el.innerText;
            const textNode = document.createTextNode(content);
            $el.parentElement.replaceChild(textNode, $el);
        });

        return $block;
        
    };

    //checkRelation: check the presence of the two brackets for highlight and the presence of the note block for a relation identified by relID.
    //if there is something missing, remove the highlight and the reference from the note (if present) and return the bloc where the highlight was if present (for saving). 
    const checkRelation = (relID, $parentFrom = document, $parentTo = document) => {

        const $openBracket = ($parentFrom != null)?
            $parentFrom.querySelector(`.bracket-open[data-rel-id='${relID}']`):
            null;

        const $closeBracket = ($parentFrom != null)?
            $parentFrom.querySelector(`.bracket-close[data-rel-id='${relID}']`):
            null;

        const $noteRelation = ($parentTo != null)?
            $parentTo.querySelector(`.block-relation[data-rel-id='${relID}']`):
            null;

        

        if($openBracket == null || $closeBracket == null || $noteRelation == null){//missing bracket, disabling relation
            removeRelation(relID, $parentFrom, $parentTo);
            return false;   
        }
        return true;


    }

    const removeRelation = (relID, $parentFrom = document, $parentTo = document) => {
        
        const $noteRelation = ($parentTo != null)?
            $parentTo.querySelector(`.block-relation[data-rel-id='${relID}']`):
            null;
        
        let $modifiedBlock = null;//need to return the block where the brackets were removed from.

        if($parentFrom != null){
            $modifiedBlock = removeHighlight(relID, $parentFrom);
        }
        if($noteRelation != null){
            $noteRelation.removeAttribute('data-from');
            $noteRelation.removeAttribute('data-to');
            $noteRelation.removeAttribute('data-n');
            $noteRelation.innerHTML = '';
        }

        if($modifiedBlock != null)
            return $modifiedBlock;
        
        else return false;

    }

    //setRelations: add labels and attributes to highlights and notes + number them
    const setRelations = ($parent = document) => {
        console.log('setRelations');
        
        const $closeBrackets = $parent.querySelectorAll('.bracket-close');
        const $highlightRelations = $parent.querySelectorAll('.highlight-relation');
        [...$highlightRelations].forEach($highlightRelation => {
            $highlightRelation.parentElement.removeChild($highlightRelation);
        });
        [...$closeBrackets].forEach($closeBracket => {
            const relID = $closeBracket.getAttribute('data-rel-id');
            setRelation(relID, $parent);
        });

    }

    const setRelation = (relID, $parentFrom = document, $parentTo = document) => {
        const $closeBracket = $parentFrom.querySelector(`.bracket-close[data-rel-id='${relID}']`);
        const $padFrom = $closeBracket.closest('.pad');

        const $noteRelation = $parentTo.querySelector(`.block-relation[data-rel-id='${relID}']`);
        if($noteRelation != null){
            const $padTo = $noteRelation.closest('.pad');
            const padToID = $padTo.getAttribute('data-id');
            const padFromID = $padFrom.getAttribute('data-id');
            const $highlightRelation = document.createElement('span');
            
            $highlightRelation.setAttribute('data-rel-id', relID);
            $highlightRelation.classList.add('highlight-relation', 'relation');

            $closeBracket.setAttribute('data-to', padToID);
            
            [$noteRelation, $highlightRelation].forEach($relation => {
                

                $relation.setAttribute('data-to', padToID);
                $relation.setAttribute('data-from', padFromID);
               
                $relation.innerHTML = 
                `<span class="rel-from">`+
                    `<span class="full-name">${$padFrom.getAttribute('data-name')}</span>`+
                    `<span class="short-name">${$padFrom.getAttribute('data-shortname')}</span>`+
                `</span>`+
                `<span class="rel-to">`+
                    `<span class="full-name">${$padTo.getAttribute('data-name')}</span>`+
                    `<span class="short-name">${$padTo.getAttribute('data-shortname')}</span>`+
                `</span>`+
                `<span class="rel-n"></span>`;
            });
        
            
            $closeBracket.parentElement.insertBefore($highlightRelation, $closeBracket.nextSibling);


            //update N info.
            //get all the $highlightRelations with the same from-to and n set.
            const $sameRelations = [...$padFrom.querySelectorAll(`.highlight-relation[data-to='${padToID}']`)];
            const relationN = $sameRelations.indexOf($highlightRelation) + 1;

            [$noteRelation, $highlightRelation].forEach($relation => {
                $relation.querySelector('span.rel-n').innerText = relationN;
                $relation.setAttribute('data-n', relationN);

            });


            if(relationN < $sameRelations.length){
                for(let i = relationN; i < $sameRelations.length; i++){
                    const thisRelID = $sameRelations[i].getAttribute('data-rel-id');
                    const $thisNoteRel = document.querySelector(`.block-relation[data-rel-id='${thisRelID}']`);
                    [$thisNoteRel, $sameRelations[i]].forEach($relation => {
                        $relation.querySelector('span.rel-n').innerText = i + 1;
                        $relation.setAttribute('data-n', i + 1)
                    });
                }
            }

            const $selectionsRelations = document.querySelectorAll(`[data-rel-id='${relID}']`);
            [...$selectionsRelations].forEach($el => {
                let timer;
                $el.addEventListener('mouseenter', e => {
                    timer = setTimeout(() => {
                        showRelation(relID, $el);
                    }, 200);
                });
             
                $el.addEventListener('mouseleave', e => {
                    clearTimeout(timer);
                    hideRelation(relID);
                });
                
            });
            
            
        }else{
            //no note found!
            console.log('no note found for relID', relID);
            removeHighlight(relID, $padFrom);
        }




    };

    const showRelation = (relID, $triggerElem = null) => {

        const $selectionsRelations = document.querySelectorAll(`[data-rel-id='${relID}']`);
        let scrolled = false;
        [...$selectionsRelations].forEach($el => {
            $el.classList.add('active');
            if($el.closest('.pad') != $triggerElem.closest('.pad') && !scrolled){
                scrolled = true;
                $el.scrollIntoView({'behavior':'smooth', 'block':'center'});
            }
        });


    }
    const hideRelation = (relID) => {
        const $selectionsRelations = document.querySelectorAll(`[data-rel-id='${relID}']`);
        [...$selectionsRelations].forEach($el => {
            $el.classList.remove('active');
        });

    }
   
    return ({init, buildInterface, insertBlock});
};
export default viewer;