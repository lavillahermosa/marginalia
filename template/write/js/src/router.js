const router = () => {

    const init = () => {
        console.log('router init');
    };

    const loadInterface = async(url) => {
        return request(window.location.href, {'action':'loadInterface', 'url':url});
    };

    const insertBlock = async(url, data, relation, parent, n) => {
        return request(window.location.href, {'action':'insertBlock', 'url':url, 'data':data, 'relation':relation, 'parent':parent, 'n':n});
    }
    
    const editBlock = async(url, data, parent, id) => {
        return request(window.location.href, {'action':'editBlock', 'url':url, 'data':data, 'parent':parent, 'id':id});

    };

    const deleteBlock = async(url, parent, id) => {
        return request(window.location.href, {'action':'deleteBlock', 'url':url, 'parent':parent, 'id':id});
    };
    
    const sortBlocks = async(url, data, parent) => {
        return request(window.location.href, {'action':'sortBlocks', 'url':url, 'data':data, 'parent':parent});
    };

    const resizePads = async(url, data) => {
        return request(window.location.href, {'action':'resizePads', 'url':url, 'data':data});
    };

    const padSetting = async(url, data, parent) => {
        return request(window.location.href, {'action':'padSetting', 'url':url, 'data':data, 'parent':parent});
    };
    
    const request = async(requestUrl, postData = {}, loader = false) => {
        // -[ ] adapt loader
        if(loader){
            var $loader = document.createElement('div');
            $loader.setAttribute('id', 'loader');
            let $itemPanel = document.querySelector('.item-panel');
            if ($itemPanel!=null)
                $itemPanel.appendChild($loader);
        }
    
        let response = await fetch(requestUrl, {
          method: 'POST', // *GET, POST, PUT, DELETE, etc.
          mode: 'cors', // no-cors, *cors, same-origin
          cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
          credentials: 'same-origin', // include, *same-origin, omit
          headers: {
            'X-Requested-With': 'XMLHttpRequest',
            'Content-Type': 'application/json'
            // 'Content-Type': 'application/x-www-form-urlencoded',
          },
          redirect: 'follow', // manual, *follow, error
          referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
          body: JSON.stringify(postData) // body data type must match "Content-Type" header*/
        });
    
        

        return response.json(); // parses JSON response into native JavaScript objects
    }

    return({init, loadInterface, insertBlock, editBlock, deleteBlock, sortBlocks, resizePads, padSetting});
};

export default router;