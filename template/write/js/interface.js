/*WRITING: INTERFACE.JS*/
import router from './src/router.js';
import viewer from './src/viewer.js';

const routes = router();

const view = viewer({
    onLoadInterface:async(itfURL) => {
        const data = await routes.loadInterface(itfURL);
        view.buildInterface(data['response']);

    },
    onAddBlock:async(itfURL, data, relation, parent, n, $loader) => {
        const dataResp = await routes.insertBlock(itfURL, data, relation, parent, n);
        $loader.parentElement.removeChild($loader);

        view.insertBlock(dataResp['response']['content'], dataResp['response']['parent'], dataResp['response']['n']);

    },
    onEditBlock:async(itfURL, data, parent, id, $block)=>{
        $block.classList.add('loader');
        const dataResp = await routes.editBlock(itfURL, data, parent, id);
        $block.classList.remove('loader');


    },
    onDeleteBlock:async(itfURL, parent, id) => {
        const dataResp = await routes.deleteBlock(itfURL, parent, id);
        console.log(dataResp);
    },
    onBlocksSort:async(itfURL, data, parent) => {
        const dataResp = await routes.sortBlocks(itfURL, data, parent);
        console.log(dataResp);
    },
    onResizePads:async(itfURL, data) => {
        const dataResp = await routes.resizePads(itfURL, data);
        console.log(dataResp);
    },
    onPadSetting:async(itfURL, data, parent) => {
        const dataResp = await routes.padSetting(itfURL, data, parent);
        console.log(dataResp);
    }
});

view.init();
routes.init();