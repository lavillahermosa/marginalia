
<?php if(!isset($marginalia)){//no direct access
    header('Location: ../');
    exit();
}
?>


<!doctype html>
<html>
    <head>
        <title>Marginalia: login</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="<?= $config->rootHttp ?>/template/login/css/login.css">
    </head>
<body>
    <main>
        <h2>marginalia: login</h2>
        <form method="POST">
            <input type="text" name="username" placeholder="user">
            <input type="password" name="password" placeholder="password">
            <input type="submit" value="login">
        </form>
    </main>
</body>
</html>