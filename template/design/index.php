<?php
  function typoRefineFr($src){
    $src = typoRefine($src);
    $src = str_replace(array('http:', 'https:'), array('http--', 'https--'), $src);
    $src = preg_replace('/([0-9]+):([0-9]+):([0-9]+)/', '$1--$2--$3', $src);
    $src = preg_replace( '/(\s?([:;!?])\s?)(?![^<>[\]]*(\]|>))/', '<span style="white-space:nowrap">&#8239;</span>$2 ', $src );
    $src = str_replace(array('? )', '! )', '? !', '! ?', '? :'), array('?)', '!)', '?!', '!?', '?:'), $src);
    $src = str_replace(array('http--', 'https--'), array('http:', 'https:'), $src);
    $src = preg_replace('/([0-9]+)--([0-9]+)--([0-9]+)/', '$1:$2:$3', $src);
    $src = preg_replace( '/«\s?(.*?)\s?»/', '«<span style="white-space:nowrap">&#8239;</span>$1<span style="white-space:nowrap">&#8239;</span>»', $src );
    return $src;
  }
  function typoRefine($src){
    $src = str_replace('...', '…', $src);
    return  html_entity_decode($src);
  }
  $q =  json_decode($marginalia->query(['action' => 'loadInterface', 'url' => $itfURL], 'design'));

/*  require_once('vendor/autoload.php');

  
  $contentURL = $q->response->pads[0]->pad_url;
  $noteURL = $q->response->pads[1]->pad_url;
  $contentMD = file_get_contents('https://mgpads.lvh0.com/p/'.$contentURL.'/export/markdown');
  $noteMD = file_get_contents('https://mgpads.lvh0.com/p/'.$noteURL.'/export/markdown');
  $Parsedown = new ParsedownExtra();
  $contentHTML = $Parsedown->text($contentMD);
  $notesHTML = $Parsedown->text($noteMD);
  */
?>


<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Layout marginalia</title>
    <script>
      const sheets = ['<?= $config->rootHttp ?>/template/design/css/interface.css',
        '<?= $config->rootHttp ?>/template/design/css/paged.css'  
      ];
    </script>
   
    <script type="module" src="<?= $config->rootHttp ?>/template/design/js/interface.js" defer></script>
    <link rel="stylesheet" href="<?= $config->rootHttp ?>/template/design/css/paged.css">
    <link rel="stylesheet" href="<?= $config->rootHttp ?>/template/design/css/interface.css">
    <?php
      $q = json_decode($marginalia->query(['action' => 'loadHeader', 'url' => $itfURL], 'design'));
      echo $q->response->content;
    ?>
  </head>
  <body>

  <?php
    $q =  json_decode($marginalia->query(['action' => 'loadInterface', 'url' => $itfURL], 'design')); 
    echo $q->response->content;

  ?>
  
  </body>
</html>
