/*DESIGN: VIEWER.JS*/

import { Previewer } from "./paged.esm.js";

const viewer = ({
    onItfSetting = null
} = {}) => {

    //lock interactions if something is already going on
    let actionInProgress = false;
    const itfURL = new URL(window.location).pathname.split('/').filter(Boolean).pop();

    const $main = document.querySelector('main');

    const init = async() => {
        initButtons(document);
        const displayMode = $main.getAttribute('data-display-mode');
        if(displayMode == 2)
            layoutIndentedNotes();
        doPage();

    };


    //BUTTONS HANDLING
    const initButtons = ($element) => {

        let $buttons;
        if($element.classList && $element.classList.contains('action'))
            $buttons = [$element];
        else
            $buttons = $element.querySelectorAll('.action');

        for(let $button of $buttons){

            const action = $button.getAttribute('data-action');
            const id = $button.getAttribute('data-id');
            const parent = $button.getAttribute('data-parent');
            const actionType = $button.getAttribute('data-action-type');
            
            
            if(action != null){
                
                $button.addEventListener(actionType, (e) => {
                    if(actionInProgress){
                        return;
                    }

                    e.stopPropagation();
                    
                    triggerAction(action, id, parent, $button);
                });
            }
            
        }
    };
    const triggerAction = (action, id, parent, $elem) => {
        switch(action){
            case 'settings':
                console.log('start settings');
                
                if($elem.classList.contains('active')){
                    actionHideSettings();
                    $elem.classList.remove('active');
                }else{
                    actionShowSettings();
                    $elem.classList.add('active');
                }
                
                break;
            case 'setCSS':
                console.log('set css');
                actionInProgress = true;
                actionSetCSS($elem.previousElementSibling.value);
                break;
            case 'setDisplayMode':
                console.log('set display mode');
                actionInProgress = true;
                actionSetDisplayMode($elem.value);
                break;
        }
    }
     /////////////////////////////////////////////////////////////////////////////////
    //ACTIONS
    //action can be triggered by an event (in case of triggerAction) or by another action (in case of chained actions)


    //actionSettings: show and hide settings window for pad
    const actionShowSettings = () => {
        console.log('show settings');
        const $settings = document.querySelector(`div.settings`);
        console.log($settings);
        $settings.classList.add('active');
       
    };
    const actionHideSettings = () => {
        const $settings = document.querySelector(`div.settings`);
        $settings.classList.remove('active');
       
    };

    const actionSetCSS = async(css) => {
        console.log(itfURL);

        await onItfSetting?.(itfURL, {'key':'css', 'value':css});
        location.reload()
    };
    const actionSetDisplayMode = async(value) => {
        console.log(itfURL);

        await onItfSetting?.(itfURL, {'key':'displayMode', 'value':value});
        location.reload()
    };
    //convert to paged.js
    const doPage = () => {
        const paged = new Previewer();
        const content = $main.innerHTML;
        $main.innerHTML = '';
        const flow = paged.preview(content, false, $main).then((flow) => {
        console.log("Rendered", flow.total, "pages.");
        });
    
    };
  
    const convertPToDivs = () => {
        
        const $ps = $main.querySelectorAll('p');
        $ps.forEach($p => {
            const $div = document.createElement('div');
            $div.classList.add('paragraph');
            $div.innerHTML = $p.innerHTML;
            $p.insertAdjacentElement('afterend', $div);
            $p.remove();
        });
        
    };
  
    const insertNotes = ($block) => {
        const $closeBrackets = $block.querySelectorAll('.bracket-close');
  
        $closeBrackets.forEach($closeBracket => {
        const relID = $closeBracket.getAttribute('data-rel-id');
        const $relation = document.querySelector(`.relation[data-rel-id='${relID}']`);
  
        if($relation != null){
            const $noteBlock = $relation.parentElement;
            // console.log($noteBlock);
            $closeBracket.insertAdjacentElement('afterend', $noteBlock);
            insertNotes($noteBlock);
        }
      
    });
  };
  
  const layoutIndentedNotes = () => {
    console.log('indented layout');
        convertPToDivs();
        const $pads = document.querySelectorAll('.pad');
        const $header = $main.querySelector('header');
        const $output = document.createElement('main');
        //on va parcourir chaque bloc de chaque pad. si le bloc n'est pas une note, on l'affiche.
        //on recherche ensuite les .bracket close et on va recherche le bloc.note dont l'id correspond pour l'intercaler
        $pads.forEach($pad => {
            const $blocks = $pad.querySelectorAll(`.block:not(.note)`);
            $blocks.forEach($block => {
                $output.append($block);
        
                insertNotes($block);
            });
        });
    
        $main.innerHTML = $output.innerHTML;
        if($header != null){
        $main.prepend($header);
        }
    
    }

    return {init};


};

export default viewer;