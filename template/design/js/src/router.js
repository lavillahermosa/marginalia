/* DESIGN: ROUTER.JS */
const router = () => {

    const init = ()  => {
        console.log('router init');
    };

    const itfSetting = async(url, data) => {
        return request(window.location.href, {'action':'itfSetting', 'url':url, 'data':data});
    };

    const request = async(requestUrl, postData = {}, loader = false) => {
        // -[ ] adapt loader
        if(loader){
            var $loader = document.createElement('div');
            $loader.setAttribute('id', 'loader');
            let $itemPanel = document.querySelector('.item-panel');
            if ($itemPanel!=null)
                $itemPanel.appendChild($loader);
        }
    
        let response = await fetch(requestUrl, {
          method: 'POST', // *GET, POST, PUT, DELETE, etc.
          mode: 'cors', // no-cors, *cors, same-origin
          cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
          credentials: 'same-origin', // include, *same-origin, omit
          headers: {
            'X-Requested-With': 'XMLHttpRequest',
            'Content-Type': 'application/json'
            // 'Content-Type': 'application/x-www-form-urlencoded',
          },
          redirect: 'follow', // manual, *follow, error
          referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
          body: JSON.stringify(postData) // body data type must match "Content-Type" header*/
        });
    
        

        return response.json(); // parses JSON response into native JavaScript objects
    }

    return {init, itfSetting};
};

export default router;