/*DESIGN: INTERFACE.JS*/
import router from './src/router.js';
import viewer from './src/viewer.js';

const routes = router();

const view = viewer({
  //onAction:async(itfURL) => {
    // const data = await routes.loadInterface(itfURL);
    // view.buildInterface(data['response']);
  //}
  onItfSetting:async(itfURL, data) => {
    const dataResp = await routes.itfSetting(itfURL, data);
    console.log(dataResp);
  }
});

view.init();
routes.init();