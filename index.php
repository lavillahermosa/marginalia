<?php 


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

session_start();

//print_r($_GET);

$parameters = explode('/', $_GET['rq']);

//print_r($parameters);


require('vendor/autoload.php');


use Marginalia;
use Monolog\Logger;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Processor\IntrospectionProcessor;
use Monolog\Formatter\LineFormatter;


//LOGGER INIT
$logger  = new Logger('applog');
$handler = new RotatingFileHandler('logs/marginalia.log', 0, Logger::INFO);
$handler->setFormatter(new LineFormatter("[%datetime%] %channel%.%level_name%: %message% %extra% %context% \n"));
$logger->pushHandler($handler);
$logger->pushProcessor(new IntrospectionProcessor);



//CORPUS INIT
$marginalia = new Marginalia\Corpus($logger);
$config = Marginalia\Corpus::buildConfig('config.php', __DIR__);
$marginalia->init($config);



//ROUTER


$contentType = trim($_SERVER["CONTENT_TYPE"] ?? '');


//CHECK AUTH

$loggedIn = false;

if(isset($_SESSION['login']) && isset($_SESSION['login']['ip'])){
    if($_SERVER['REMOTE_ADDR'] == $_SESSION['login']['ip']){
        $loggedIn = true;
    }
}
if(isset($_POST['username']) && isset ($_POST['password'])){
    $testLog = $marginalia->checkUser($_POST['username'], $_POST['password']);
    if($testLog){
        $loggedIn = true;
        $_SESSION['login'] = ['ip' => $_SERVER['REMOTE_ADDR'], 'user' => $_POST['username']];

    }
}

if(!$loggedIn){

    if($contentType === 'application/json'){
        echo json_encode(array('result' => 'ko', 'message' => 'please login'));
    }
    else{
        include('template/login/index.php');
    }
    exit();
}


if(strpos($contentType, 'multipart/form-data;') !== false){
    if(isset($_SERVER['HTTP_UPLOADTYPE']) 
    && $_SERVER['HTTP_UPLOADTYPE'] == 'image'
    && isset($_FILES['upload'])){
        echo $marginalia->uploadImage($_FILES['upload']);
    }
}
else if($contentType === 'application/json'){
    //IF AJAX
    $logger->info('got ajax query');
    try{
        $content = trim(file_get_contents("php://input"));
        $request = json_decode($content, true);
        if (is_null($request)) {
            throw ('json object is null');
        }
    }catch (Exception $e) {
        $logger->error('bad query: '.$e->getMessage());
    }

    echo $marginalia->query($request, $parameters[0]);

}else{
    //IF NOT AJAX
    switch($parameters[0]){
        case 'ctrl':
            require('template/admin/index.php');
            break;
        case 'write':
            if(count($parameters) >= 2 && preg_match('#^itf-[a-z0-9]+$#', $parameters[1])){
                $itfURL = $parameters[1];
                require('template/write/index.php');
            }
        
            break;
        case 'design':
            
            if(count($parameters) >= 2 && preg_match('#^itf-[a-z0-9]+$#', $parameters[1])){
                $itfURL = $parameters[1];
                
                require('template/design/index.php');
            }
        
            break;
        default:
            header('Location: '.$config->rootHttp.'/ctrl');
            die();
    }
}
