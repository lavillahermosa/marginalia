# marginalia

**WORK IN PROGRESS !**

Marginalia is a writing tool.

**TODO**
* ~~admin: implement clone interface~~
* ~~admin: implement lock pad~~
* ~~admin: implement delete pad + delete interface + delete chapter~~
* ~~main: password protect everything~~
* writing: add ui zone for interface (side pannel)
* writing: implement versioning. save version + restore version
* writing: backup interface (save all pads in archive file)
* add custom styles for editor in config
* admin: implement clone pads + clone chapters

**TODO IN THE FUTURE**
* admin: password protect interfaces
* helper for interactions?
* writing: implement history of actions
* writing: multi user interface
