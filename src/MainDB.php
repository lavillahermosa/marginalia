<?php

namespace Marginalia;

class MainDB extends DB{
    

    protected $dbStruct = [
        'PRAGMA foreign_keys=on',

        'CREATE TABLE IF NOT EXISTS users (
            user_id INTEGER PRIMARY KEY,
            user_name TEXT,
            user_password TEXT
        )',

        'CREATE TABLE IF NOT EXISTS chapters (
            chapter_id   INTEGER PRIMARY KEY,
            chapter_name TEXT NOT NULL,
            n INTEGER NOT NULL
          )',
        
        'CREATE TABLE IF NOT EXISTS interfaces (
            interface_id INTEGER PRIMARY KEY,
            interface_settings TEXT NOT NULL,
            interface_name TEXT NOT NULL,
            interface_url VARCHAR(512) NOT NULL,
            n INTEGER NOT NULL,
            parent_id INTEGER NOT NULL,
                FOREIGN KEY (parent_id)
                REFERENCES chapters(chapter_id) ON UPDATE CASCADE
                                                ON DELETE CASCADE
        )',
        'CREATE TABLE IF NOT EXISTS pads (
            pad_id INTEGER PRIMARY KEY,
            pad_settings TEXT NOT NULL,
            pad_name  TEXT NOT NULL,
            pad_url VARCHAR(512) NOT NULL,
            n INTEGER NOT NULL,
            parent_id INTEGER NOT NULL,
            FOREIGN KEY (parent_id)
            REFERENCES interfaces(interface_id) ON UPDATE CASCADE
                                            ON DELETE CASCADE)',

        'INSERT INTO users(user_name) VALUES (\'marginalia\')'
    ];

    private $tableToFieldName = ['chapters' => 'chapter', 'pads' => 'pad', 'interfaces' => 'interface'];

    public function getPasswd($user){
        $stmt = $this->query('SELECT user_password FROM users WHERE user_name = :user', [':user' => $user]);
        if($stmt == false)
            return false;

        $answer = $stmt->fetch(\PDO::FETCH_ASSOC);
        return ($answer)?$answer['user_password']:false;



    }

    public function setPasswd($user, $password){
        $hash = password_hash($password, PASSWORD_DEFAULT);
        $stmt = $this->query('UPDATE users SET user_password = :password WHERE user_name = :user', [':password' => $hash, ':user' => $user]);
        if($stmt == false)
            return $false;
        return true;

    }

    public function getInterface($url){
        $stmt = $this->query('SELECT interface_name, interface_id, interface_settings, chapter_name FROM interfaces as i
                              LEFT JOIN chapters as c on i.parent_id = c.chapter_id WHERE interface_url = :url',
                              ['url' => $url]);
        if($stmt == false)
            return false;
        $answer = $stmt->fetch(\PDO::FETCH_ASSOC);
        return ($answer)?$answer:false;
        
    }

    public function getLinkedPads($itfId){
        $stmt = $this->query('SELECT pad_id, pad_settings, pad_name, pad_url, n FROM pads
                            WHERE parent_id = :itfId ORDER BY n',
                            ['itfId' => $itfId]);
        
        if($stmt !== false)
            return $stmt->fetchAll();
        
        return false;
        
    }

    public function getListing($type, $parent = null){
        if($parent !== null){
            $stmt = $this->query('SELECT * FROM '.$type.' WHERE parent_id = :parent ORDER BY n', [
                ':parent' => $parent
            ]);
        }else{
            $stmt = $this->query('SELECT * FROM '.$type.' ORDER BY n');
        }
        if($stmt !== false)
            return $stmt->fetchAll();

        return false;
    }

    public function copyListingRow($type, $data, $oldID, $parent = false){

        if($parent != null)
            $n = $this->getNextN($type, ['field' => 'parent_id', 'value' => $parent]);
        else
            $n = $this->getNextN($type);


        switch($type){
            case 'interfaces':
                $url = $this->createInterfaceURL();
                $stmt = $this->query('INSERT INTO interfaces(interface_settings, interface_name, interface_url, n, parent_id) 
                    SELECT interface_settings, :name, :url, :n, :parent FROM interfaces WHERE interface_id = :id', [
                        'name' => $data['name'],
                        'url' => $url,
                        'n' => $n,
                        'parent' => $parent,
                        'id' => $oldID
                    ]);
                if($stmt === false)
                    return false;
                
                $id = $this->getInsertedID();

                $oldPads = $this->getLinkedPads($oldID);
                
                foreach($oldPads as $oldPad){
                    $this->copyListingRow('pads', [], $oldPad['pad_id'], $id);
                }
                
                break;

            case 'pads':
        
                $padURL = $this->createPadURL();
                $oldPadURL = $this->getPadURL($oldID);

                
                $stmt = $this->query('INSERT INTO pads(pad_settings, pad_name, n, pad_url, parent_id)
                 SELECT pad_settings, pad_name, n, :url, :parent_id FROM pads WHERE pad_id = :id', [
                    'url' => $padURL, 'parent_id' => $parent, 'id' => $oldID]);

                if($stmt == false)
                    return false;
                
                $dataPath = $this->config->padsDataPath.
                    ((substr($this->config->padsDataPath, -1) != '/')?'/':'').$padURL;
                $oldDataPath = $this->config->padsDataPath.
                    ((substr($this->config->padsDataPath, -1) != '/')?'/':'').$oldPadURL;
                
                mkdir($dataPath);
                
                $oldDBPath = $oldDataPath.'/.'.$oldPadURL.'.db';
                $dbPath = $dataPath.'/.'.$padURL.'.db';

                if(!copy($oldDBPath, $dbPath))
                    return false;

                break;


        }

        return true;
    }

    public function deleteListingRow($type, $id){

        //first get n
        switch($type){
            case 'pads':
            case 'interfaces':
                $stmt = $this->query('SELECT n, parent_id FROM '.$type.' WHERE '.$this->tableToFieldName[$type].'_id = :id', [':id' => $id]);
                break;
            case 'chapters':
                $stmt = $this->query('SELECT n FROM '.$type.' WHERE '.$this->tableToFieldName[$type].'_id = :id', [':id' => $id]);
                break;

        }
        
        if($stmt == false)
            return false;

        $answer = $stmt->fetch(\PDO::FETCH_ASSOC);

        if(isset($answer['parent_id']))
            $parent = $answer['parent_id'];

        $n = $answer['n'];

        

        switch($type){
            case 'chapters':
            case 'interfaces':
                $childrenType = ($type == 'chapters')?'interfaces':'pads';
                $children = $this->getListing($childrenType, $id);
                foreach($children as $child){
                    $this->deleteListingRow($childrenType, $child[$this->tableToFieldName[$childrenType].'_id']);
                }   
                break;

            case 'pads':
                
                $padURL = $this->getPadURL($id);
                if(!$padURL){
                    $this->logger->info('unable to retrieve pad url');
                    return;
                }
                $dataPath = $this->config->padsDataPath.
                    ((substr($this->config->padsDataPath, -1) != '/')?'/':'').$padURL;
                $dbPath = $dataPath.'/.'.$padURL.'.db';
                if(is_file($dbPath)){
                    unlink($dbPath);
                }else{
                    $this->logger->info('no db file to delete');
                }

                if(is_dir($dataPath)){
                    rmdir($dataPath);
                }else{
                    $this->logger->info('no db directory to delete');
                }
                break;

                

                

        }

        $stmt = $this->query('DELETE FROM '.$type.' WHERE '.$this->tableToFieldName[$type].'_id = :id', [':id' => $id]);
        if($stmt === false)
            return false;



        $args = [':n' => $n];

        if(isset($parent)){
            $query = 'UPDATE '.$type.' SET n = n-1 WHERE n >= :n AND parent_id = :parent';
            $args[':parent'] = $parent;
        }
        else{
            $query = 'UPDATE '.$type.' SET n = n-1 WHERE n >= :n';
        }
        
        $stmt = $this->query($query, $args);
        if($stmt === false) return false;

        return true;

    }

    public function setListingRow($type, $data, $id){
        $settings = $this->getRowSettings($type, $id);
        
        
        foreach($data as $key => $value){
                $settings->$key = $value;
        }

        $this->saveRowSettings($type, $id, $settings);

    }

    private function updateListingRow($type, $data, $id, $parent){

        if(isset($data['name'])){
            $this->logger->info('update name of listing row');
            $fieldName = $this->tableToFieldName[$type];
            $stmt = $this->query('UPDATE '.$type.' SET '.$fieldName.'_name = :name WHERE '.$fieldName.'_id = :id', [
                'name' => $data['name'],
                'id' => $id
            ]);
            if($stmt === false) return false;
        }

        
        return true;
    
        
    }
    private function getNextN($type, $condition = null){
        if($condition != null)
            $stmt = $this->query('SELECT MAX(n) as n FROM '.$type.' WHERE '.$condition['field'].' = :condVal', [
                ':condVal' => $condition['value']
            ]);
        else
            $stmt = $this->query('SELECT MAX(n) as n FROM '.$type);

        if($stmt === false)
            return false;
        $answer = $stmt->fetch(\PDO::FETCH_ASSOC);
        $n = ($answer)?$answer['n'] + 1:1;
    
        $this->logger->info('next n = '.$n);

        return $n;
                
    }
    private function createPadURL(){
        return uniqid('pad-');
    }
    private function createInterfaceURL(){
        return uniqid('itf-');
    }

    private function getPadURL($id){
        $stmt = $this->query('SELECT pad_url FROM pads where pad_id = :id', ['id' => $id]);
        if($stmt === false)
            return false;
        
        $answer = $stmt->fetch(\PDO::FETCH_ASSOC);

        return ($answer)?$answer['pad_url']:false;

    }
    private function getRowSettings($type, $id){
        $stmt = $this->query('SELECT '.$this->tableToFieldName[$type].'_settings FROM '.$type.' WHERE '.$this->tableToFieldName[$type].'_id = :id', [
            'id' => $id
        ]);
        if($stmt === false)
        return false;
    
        $answer = $stmt->fetch(\PDO::FETCH_ASSOC);

        return ($answer)?json_decode($answer[$this->tableToFieldName[$type].'_settings']):false;
    }

    private function getPadColor($parent){
        $neighboursPads = $this->getLinkedPads($parent);
        $nbPads = count($neighboursPads);
        if($nbPads == 0){
            return rand(0, 360);
        }
        $basePad = $neighboursPads[0];
        $basePadSettings = json_decode($basePad['pad_settings']);
        $baseColor = $basePadSettings->color;
        
        //from $baseColor, calculate the new color that should be as far as possible from the other colors already in the interface.
        $step = floor(log($nbPads, 2) + 1);
        $divider = pow(2, $step);
        $stepAngle = 360/$divider;
        $multiplicator = ($nbPads % ($divider / 2)) * 2 + 1;
        

        return ($baseColor + $stepAngle * $multiplicator) % 360;
        //print_r($neighboursPads);
    }
    
    private function insertListingRow($type, $data, $parent){
        if($parent != null)
            $n = $this->getNextN($type, ['field' => 'parent_id', 'value' => $parent]);
        else
            $n = $this->getNextN($type);

        if($n == false)
            return false;
        
        switch($type){
            case 'chapters':

                $stmt = $this->query('INSERT INTO chapters(chapter_name, n) VALUES(:name, :n)', 
                    array(':name' => $data['name'], ':n' => $n));

                if($stmt === false)
                    return false;

                break;
            case 'interfaces':
                $interfaceURL = $this->createInterfaceURL();
                $stmt = $this->query('INSERT INTO interfaces(interface_settings, interface_name, interface_url, n, parent_id) VALUES(:settings, :name, :url, :n, :parent_id)',
                    array(':settings' => '{}', ':name' => $data['name'], ':url' => $interfaceURL, ':n' => $n, ':parent_id' => $parent));
                    if($stmt == false)
                    return false;
                
                break;
            case 'pads':
                $padURL = $this->createPadURL();
                $padColor = $this->getPadColor($parent);
                $stmt = $this->query('INSERT INTO pads(pad_settings, pad_name, pad_url, n, parent_id) VALUES(:settings, :name, :pad_url, :n, :parent_id)',
                    array(':settings' => '{"color":'.$padColor.', "lock":false}', ':name' => $data['name'], ':pad_url' => $padURL, ':n' => $n, ':parent_id' => $parent));
                if($stmt == false)
                    return false;
                
                
                break;
            
            default:
                return false;
            
        }
        return true;
    }

    public function saveListingRow($type, $data, $id, $parent){
        $this->logger->info('save listing row type='.$type.' id= '.$id.' parent='.$parent, $data);


        if($id !== null){
            return $this->updateListingRow($type, $data, $id, $parent);
        }else{
            return $this->insertListingRow($type, $data, $parent);
        }

    }

    public function saveRowSettings($type, $id, $settings){
        $stmt = $this->query('UPDATE '.$type.' SET '.$this->tableToFieldName[$type].'_settings = :settings WHERE '.$this->tableToFieldName[$type].'_id = :id', [
            'settings' => json_encode($settings), 'id' => $id
        ]);

        if($stmt === false)
            return $false;

        return true;
    }

    public function saveListingOrder($type, $data, $parent){

        $fieldName = $this->tableToFieldName[$type];
        foreach($data as $row){
            $stmt = $this->query('UPDATE '.$type.' SET n = :n WHERE '.$fieldName.'_id = :id', [
                'n' => $row['n'], 'id' => $row['id']
            ]);
            
            if($stmt === false)
                return false;
                    
            


        }
        return true;
    }  
    


}