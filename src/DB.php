<?php

##ABSTRACT CLASS FOR DBS REQUESTS
###

namespace Marginalia;

abstract class DB {
    protected $pdo;
    private $dbPath;


    protected $logger;
    protected $dbStruct;
    protected $config;
    

    
    
    public function __construct(\Monolog\Logger $logger, $config, $dbPath){
        $this->dbPath = $dbPath;
        $this->logger = $logger;
        $this->config = $config;
    }



    public function exists(){
        return file_exists($this->dbPath);
    }

    public function connect(){
        $this->logger->info('connecting to DB: '.$this->dbPath);
        try{
            $this->pdo = new \PDO('sqlite:'.$this->dbPath, '', '', array(
                \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
            ));
        }catch(PDOException $e){
            $this->logger->error("pdo error :" . $e->getMessage());
        }
    }

    public function create(){
        $this->connect();
        $this->logger->info('building db structure');
        foreach($this->dbStruct as $cmd){
            try{
                $this->pdo->exec($cmd);
            }catch(PDOException $e){
                $this->logger->error("pdo error :" . $e->getMessage());
            }
        }


    }

    //a generic function so send query to db. 
    protected function query($qString, $parameters = null){
        $this->logger->info('sending query to db '.$qString);
        try{
            
            $stmt = $this->pdo->prepare($qString);
            if($parameters !== null)
                $stmt->execute($parameters);
            else
                $stmt->execute();

        }catch(PDOException $e){
            $this->logger->error("pdo error :" . $e ->getMessage);
            return false;
        }

        return $stmt;

    }
    protected function getInsertedID(){
        return $this->pdo->lastInsertId();
    }

}