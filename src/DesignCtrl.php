<?php
namespace Marginalia;
##Controller for design interface

class DesignCtrl {
    private $db;
    private $logger;
    private $config;

    private $url;
    private $name;
    private $id;
    private $settings;
    private $chapterName;
    public $pads;

    public function __construct(\Monolog\Logger $logger, \Marginalia\MainDB $db, \Marginalia\Config $config, $url){
        $this->logger = $logger;
        $this->db = $db;
        $this->url = $url;
        $this->config = $config;

        $itfRef = $this->db->getInterface($url);
        $this->name = $itfRef['interface_name'];
        $this->id = $itfRef['interface_id'];
        $this->settings = $itfRef['interface_settings'];
        $this->chapterName = $itfRef['chapter_name'];
        $this->pads = $this->loadLinkedPads();

        
        
       
    }


    private function loadLinkedPads(){
        $pads = [];
        $padsRefs = $this->db->getLinkedPads($this->id);

        foreach($padsRefs as $padRef){
            $pads[$padRef['pad_id']] = new PadCtrl($this->logger, $this->config, $padRef, $this);

        }

        return $pads;
    }

    private function buildHTML(){
        $settings = json_decode($this->settings);
        $css = '';
        if(isset($settings->css))
            $css = $settings->css;
        
        $displayMode = 1;

        if(isset($settings->displayMode))
            $displayMode = $settings->displayMode;

        $output = '<header class="itf-header">
                        <div class="tools"><span class="action button settings" data-action-type="click" data-id="'.$this->id.'" data-action="settings"></div>
                        
                        <div class="settings">
                            <div>
                                <label>custom css: </label><textarea>'.$css.'</textarea>
                                <button class="action" data-action="setCSS" data-action-type="click">apply</button>
                            </div>
                            <div>
                                <label>content display mode:</label>'.$displayMode.'
                                <select class="action" data-action="setDisplayMode" data-action-type="change">
                                    <option '.(($displayMode == 1)?'selected':'').' value="1">one pad after another</option>
                                    <option '.(($displayMode == 2)?'selected':'').' value="2">dicussions between text fragments</option>
                                </select>

                            </div>
                        </div>
                    </header>';

        $output .= '<main class="itf" data-display-mode="'.$displayMode.'" data-id="'.$this->id.'" data-url="'.$this->url.'">
                    <header><h1>'.$this->chapterName.' | '.$this->name.'</h1></header>';

        foreach($this->pads as $pad){
            $output .= $pad->buildHTML();
        }
        
        $output .= '</main>';
        return $output;
    }

    private function buildHTMLHeader(){
        $output = '';
        $settings = json_decode($this->settings);
        if(isset($settings->css)){
            $output .= '<style>'.$settings->css.'</style>';
             
        }
        return $output;
    }

    public function query($request){
        $response = '';
        switch($request['action']){

            case 'loadInterface':
                $htmlOutput = $this->buildHTML();
                
                $response = ['content' => $htmlOutput];
                break;
            case 'loadHeader':
                $htmlOutput = $this->buildHTMLHeader();
                $response = ['content' => $htmlOutput];
                break;
            case 'itfSetting':
                $this->db->setListingRow('interfaces', [$request['data']['key'] => $request['data']['value']], $this->id);
                break;
           
        }
        return $response;
    }
}



class PadCtrl {
    private $db;
    private $logger;
    private $config;
    private $dataPath;
    private $itfCtrl;

    public $id;
    public $settings;
    public $url;
    public $rev;
    public $n;


    public $name;

    public function __construct(\Monolog\Logger $logger, \Marginalia\Config $config, $padRef, $itfCtrl){
        $this->logger = $logger;
        $this->id = $padRef['pad_id'];
        $this->n = $padRef['n'];
        $this->settings = json_decode($padRef['pad_settings']);
        $this->name = $padRef['pad_name'];
        $this->url = $padRef['pad_url'];
        $this->itfCtrl = $itfCtrl;
    
        $this->dataPath = $config->padsDataPath.
            ((substr($config->padsDataPath, -1) != '/')?'/':'').$this->url;
        
        if(!is_dir($this->dataPath)){
            mkdir($this->dataPath);
        }
        $dbPath = $this->dataPath.'/.'.$this->url.'.db';
        $this->db = new PadDB($this->logger, $this->config, $dbPath);
        if(!$this->db->exists()){
            $this->db->create();
            $this->rev = $this->db->insertRev();
        }else{
            $this->db->connect();
            $this->rev = $this->db->getLastRev();

        }

    }

    public function buildHTMLFragment($fragment, $isLocked = false, $noHighlight = false){
        
        $relation = $fragment['fragment_relation'];
        $classList = 'block';
        $head = '';
        if($relation != null){
            $classList .= ' note';
            $head .= '
                <div class="block-relation relation" data-rel-id="'.$relation.'">
                </div>';
        }

        $output = '
            <div class="'.$classList.'" data-pad-n="'.$this->n.'" data-pad-id="'.$this->id.'" data-n="'.$fragment['n'].'" data-id="'.$fragment['fragment_id'].'">'
                .$head.
                '<div class="block-main">
                    <div class="block-content">'.$fragment['fragment_content'].'</div>
               
                   
                </div>
               
            </div>'
           ;
        

        return $output;
    }

    public function buildHTML(){
        $nbPads = count($this->itfCtrl->pads);

        $output = '
            <section class="pad" data-id="'.$this->id.'">
                
                <div class="pad-content">
            ';
        
        //here go the blocks
        $fragments = $this->db->getFragments($this->rev);
        foreach($fragments as $fragment){
            $output .= $this->buildHTMLFragment($fragment);
        }

        $output .= '
                </div>
                
            </section>';

        return $output;
    }

    public function buildCSS(){

        $output = '
            .pad[data-id=\''.$this->id.'\'] .selection{
                background-color:hsl('.$this->settings->color.', 90%, 90%);
            }
            .pad[data-id=\''.$this->id.'\'] .pad-header h2::before {
                content: \'\';
                display: inline-block;
                width: 2rem;
                margin-left:-2rem;
                height: 1rem;
                background-color:hsl('.$this->settings->color.', 90%, 90%);
            }
        ';
        foreach($this->itfCtrl->pads as $pad){
            if($pad->id == $this->id) continue;
            
            $output .= '.relation[data-from=\''.$this->id.'\'][data-to=\''.$pad->id.'\']{
                background:linear-gradient(to right, hsl('.$this->settings->color.', 90%, 90%), hsl('.$pad->settings->color.', 90%, 90%));
            }';

        }
        //.relation[data-from=\''.$this->id.'\'] span

        

        return $output;
    }
    
}