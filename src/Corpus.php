<?php
namespace Marginalia;

class Corpus {//this is the main controller of Marginalia
    public $config;
    public $db;
    private $logger;

    public function __construct(\Monolog\Logger $logger){
        $this->logger = $logger;
       
    }

    public function init($config){
        $this->logger->info('corpus init');
        $this->config = $config;

        $this->db = new MainDB($this->logger, $this->config, $this->config->dbPath);

        if(!$this->db->exists()){
            $this->db->create();
        }else{
            $this->db->connect();
        }
    }

    public static function buildConfig($configFile, $rootPath){
        $config = new Config($rootPath);
        require($configFile);
        return $config;
    }

    public function checkUser($user, $passwd){
        $dbPasswd = $this->db->getPasswd($user);
        if($dbPasswd === false){
            //user unknown
            return false;
        }
        if($dbPasswd === null){
            //no password set: enter
            return true;
        }

        if(password_verify($passwd, $dbPasswd)){
            //password ok: enter
            return true;
        }

    }



    public function uploadImage($file){
        $name = Utils::sanitizeFileName($file['name']);
        $name = Utils::renameFileIfExists($name, $this->config->imgsDataPath);

        copy($file['tmp_name'], $this->config->imgsDataPath.$name);
        return json_encode(array('url' => '../data/images/'.$name));
    }

    public function query($request, $from){
        if(!isset($request['action'])){
            $this->logger->error('no action in request');
            return json_encode(array('result' => 'ko', 'message' => 'bad request'));
        }
        
        $response = '';

        if($from == 'ctrl'){
            //this is a request from the admin.
            $ctrl = new AdminCtrl($this->logger, $this->db, $this->config);
        }
        else if($from == 'write' || $from == 'design'){
            //this is a request from the interfaces
            if(!isset($request['url']))
                return json_encode(array('result' => 'ko', 'message' => 'bad request'));
            if($from == 'write')
                $ctrl = new InterfaceCtrl($this->logger, $this->db, $this->config, $request['url']);
            else
                $ctrl = new DesignCtrl($this->logger, $this->db, $this->config, $request['url']);
        }
        else{
            return json_encode(array('result' => 'ko', 'message' => 'bad request'));
        }
        $response = $ctrl->query($request);
        
        return json_encode(array('result' => 'ok', 'response' => $response));
    }


}