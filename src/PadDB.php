<?php

namespace Marginalia;

class PadDB extends DB{
    

    protected $dbStruct = [
        'PRAGMA foreign_keys=on',

        'CREATE TABLE IF NOT EXISTS revisions (
            revision_id INTEGER PRIMARY KEY,
            revision_name TEXT,
            revision_time DATETIME DEFAULT CURRENT_TIMESTAMP
          )',
        
        'CREATE TABLE IF NOT EXISTS fragments (
            fragment_id INTEGER PRIMARY KEY,
            fragment_content TEXT NOT NULL,
            fragment_relation TEXT DEFAULT NULL,
            fragment_lock INTEGER DEFAULT 0,
            n INTEGER NOT NULL,
            parent_id INTEGER NOT NULL,
                FOREIGN KEY (parent_id)
                REFERENCES revisions(revision_id) ON UPDATE CASCADE
                                                  ON DELETE CASCADE
        )',
        'CREATE TABLE IF NOT EXISTS operations (
            operation_id INTEGER PRIMARY KEY,
            operaction_action TEXT NOT NULL,
            operation_reverse  TEXT NOT NULL,
            operation_time DATETIME DEFAULT CURRENT_TIMESTAMP,
            parent_id INTEGER NOT NULL,
            FOREIGN KEY (parent_id)
            REFERENCES revisions(revision_id) ON UPDATE CASCADE
                                            ON DELETE CASCADE)'
    ];

    private $tableToFieldName = ['revisions' => 'revision', 'fragments' => 'fragment', 'operations' => 'operation'];

    public function insertRev($name = 'init'){
        $stmt = $this->query('INSERT INTO revisions(revision_name) VALUES(:name)', array(':name'=>$name));
        if($stmt !== false){
            $revId = $this->getInsertedID();
        }
        return $revId;
    }

    public function getLastRev(){
        $stmt = $this->query('SELECT MAX(revision_id) as id FROM revisions');
        if($stmt === false)
            return false;
        $answer = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $answer['id'];
    }


    public function getFragments($rev){
        $stmt = $this->query('SELECT fragment_id, fragment_content, fragment_relation, fragment_lock, n  FROM fragments
                            WHERE parent_id = :rev ORDER BY n',
                            ['rev' => $rev]);
        
        if($stmt !== false)
            return $stmt->fetchAll();
        
        return false;
    }

    public function sortFragments($data){
        foreach($data as $row){
            $stmt = $this->query('UPDATE fragments set n = :n WHERE fragment_id = :id', ['n' => $row['n'], 'id' => $row['id']]);
            if($stmt = false)
                return false;
        }
        return true;
    }

    public function insertFragment($data, $relation, $n, $rev){

        $stmt = $this->query('UPDATE fragments set n = n+1 WHERE n >= :n AND parent_id = :rev', array(':n' => $n, 'rev' => $rev));
        if($stmt === false)
            return false;
    

        $stmt = $this->query('INSERT INTO fragments(fragment_content, fragment_relation, n, parent_id) VALUES(:data, :relation, :n, :rev)', 
                    array(':data' => $data, ':relation' => $relation, ':n' => $n, ':rev' => $rev));

        if($stmt === false)
            return false;
        
        $fragId = $this->getInsertedID();


        
        
        return array('fragment_content' => $data, 'fragment_relation' => $relation, 'n' => $n, 'fragment_id' => $fragId);

        /*$stmt = $this->query('SELECT interface_name, interface_id, interface_split_pos, chapter_name FROM interfaces as i
                              LEFT JOIN chapters as c on i.chapter_id = c.chapter_id WHERE interface_url = :url',
                              ['url' => $url]);
        if($stmt == false)
            return false;
        $answer = $stmt->fetch(\PDO::FETCH_ASSOC);
        return ($answer)?$answer:false;
        */
    }

    public function updateFragment($data, $id){
        $stmt = $this->query('UPDATE fragments set fragment_content = :data WHERE fragment_id = :id', 
            array(':data' => $data, ':id' => $id));
        
        if($stmt === false)
            return false;
        return true;

    }

    public function deleteFragment($id){
        //first get fragment revision and n
        $stmt = $this->query('SELECT parent_id, n  FROM fragments WHERE fragment_id = :id', ['id' => $id]);
        if($stmt === false)
            return false;
        
        $answer = $stmt->fetch(\PDO::FETCH_ASSOC);
        
        $n = $answer['n'];
        $rev = $answer['parent_id'];
        
        //delete fragment
        $stmt = $this->query('DELETE FROM fragments WHERE fragment_id = :id', [':id' => $id]);

        if($stmt === false)
            return false;

        //update n
        $stmt = $this->query('UPDATE fragments set n = n-1 WHERE n >= :n AND parent_id = :rev', [':n' => $n, ':rev' => $rev]);
        if($stmt === false)
            return false;



    }


    

}