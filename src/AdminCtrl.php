<?php

namespace Marginalia;

##Admin controller

class AdminCtrl {
    private $db;
    private $logger;
    private $config;

    public function __construct(\Monolog\Logger $logger, \Marginalia\MainDB $db, \Marginalia\Config $config){
        $this->logger = $logger;
        $this->db = $db;
        $this->config = $config;
       
    }

    private function buildHtml($result, $type){
        $output = '';
        //print_r($result);
        foreach($result as $row){
            $output .= '<li data-id="'.$row[0].'">';

            switch($type){
                case 'chapters':
                    $output .= '<div class="action name" data-action="rename" data-type="'.$type.'" data-id="'.$row['chapter_id'].'">'.
                                    $row['chapter_name'].
                               '</div>';
                    
                    $output .= '<div class="tools">
                                    <span class="action button go" data-action="select" data-type="chapters" data-id="'.$row[0].'"></span>
                                    <span class="action button delete" data-action="delete" data-type="chapters" data-id="'.$row[0].'"></span>
                                    <span class="action button move" data-id="'.$row[0].'" data-action="move" data-type="chapters"></span>
                                    
                                </div>';
                    break;



                
                case 'interfaces':
                    
                    $output .= '<div class="action name" data-action="rename" data-parent="'.$row['parent_id'].'" data-type="'.$type.'" data-id="'.$row['interface_id'].'">'.
                                    $row['interface_name'].
                                '</div>';

                    $output .= '<div class="tools">
                                    <span class="action button go" data-action="select" data-type="interfaces" data-id="'.$row[0].'"></span>
                                    <span class="action button delete" data-action="delete" data-type="interfaces" data-id="'.$row[0].'"></span>
                                    <span class="action button copy" data-parent="'.$row['parent_id'].'" data-id="'.$row[0].'" data-action="copy" data-type="interfaces"></span>
                                    <span class="action button move" data-parent="'.$row['parent_id'].'" data-id="'.$row[0].'" data-action="move" data-type="interfaces"></span>
                                     <span class="delimiter"></span>
                                    <span class="action button design" data-url="'.$row['interface_url'].'" data-id="'.$row['interface_id'].'" data-action="design" data-type="interfaces"></span>
                                    <span class="action button write" data-url="'.$row['interface_url'].'" data-id="'.$row['interface_id'].'" data-action="write" data-type="interfaces"></span>
                                    
                                </div>';
                    break;
                case 'pads':
                    $settings = json_decode($row['pad_settings']);

                    $padLocked = (isset($settings->lock) && $settings->lock == true)?true:false; 

                    $output .= '<div class="action name" data-action="rename" data-parent="'.$row['parent_id'].'" data-type="'.$type.'" data-id="'.$row['pad_id'].'">'.
                                    $row['pad_name'].
                                '</div>';

                    $output .= '<div class="tools">
                                    <span class="action'.(($padLocked)?' locked ':' ').'button lock" data-id="'.$row['pad_id'].'" data-action="lock" data-type="pads"></span>
                                    <span class="action button delete" data-action="delete" data-type="pads" data-id="'.$row[0].'"></span>
                                    <span class="action button move" data-parent="'.$row['parent_id'].'" data-id="'.$row[0].'" data-action="move" data-type="pads"></span>
                                    
                                </div>';
                    break;

            }

            $output .= '</li>';

        }
        return $output;
    }

    public function query($request){
        $response = '';
        switch($request['action']){
            case 'loadListing':
                $listing = $this->db->getListing($request['type'], $request['parent']);
                $response = $this->buildHtml($listing, $request['type']);
                break;
            case 'saveListingRow':
                $this->db->saveListingRow($request['type'], $request['data'], $request['id'], $request['parent']);
                $listing = $this->db->getListing($request['type'], $request['parent']);
                $response = $this->buildHtml($listing, $request['type']);
                break;
            case 'copyListingRow':
                $this->db->copyListingRow($request['type'], $request['data'], $request['id'], $request['parent']);
                $listing = $this->db->getListing($request['type'], $request['parent']);
                $response = $this->buildHtml($listing, $request['type']);
                break;
            case 'setListingRow':
                $this->db->setListingRow($request['type'], $request['data'], $request['id']);
                break;
            case 'deleteListingRow':
                $this->db->deleteListingRow($request['type'], $request['id']);
                break;
                
            case 'saveListingOrder':
                $this->db->saveListingOrder($request['type'], $request['data'], $request['parent']);
                break;
            case 'setPassword':
                if(isset($_SESSION['login']) && isset($_SESSION['login']['user'])){
                    if($this->db->setPasswd($_SESSION['login']['user'], $request['data'])){
                        session_destroy();
                    }
                }
                break;
            
        }
        return $response;
    }
}