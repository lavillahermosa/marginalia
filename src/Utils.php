<?php

namespace Marginalia;

class Utils{
    public static function sanitizeFileName($fileName) {
        $arr = ['?','[',']','/','\\','=','<','>',':',';',',', "'",'"','&','$','#','*','(',')','|','~','`','!','{','}','%','+','’','«','»','”','“'];
        $info = pathinfo($fileName);
        $name = $info['filename'];
        $ext  = $info['extension'];
        
        $name = str_replace( $arr, '', $name );
        $name = preg_replace( '/[\. _-]+/', '-', $name );
        $name = trim( $name, '-' );
        
        return $name.'.'.$ext; 
    }
    public static function renameFileIfExists($name, $dir) {
        $file = $dir.DIRECTORY_SEPARATOR.$name;
        $info = pathinfo($file);
        $i = 0;
        while ( file_exists($file) ) {
         $i++;
         $file = $info['dirname'] .DIRECTORY_SEPARATOR
                .$info['filename'].$i.'.'
                .$info['extension'];  
        }
        return pathinfo($file, PATHINFO_BASENAME);
    }
    
}