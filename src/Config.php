<?php
namespace Marginalia;


class Config{
    public $rootPath;
    public $padsDataPath;
    public $imgsDataPath;
    public $dbPath;
    public $projectName;
    public $padsURL;
    public $rootHttp;

    public function __construct($rootPath){
       $this->rootPath = $rootPath;
      // print_r($_SERVER);
       $this->rootHttp =  (dirname($_SERVER['PHP_SELF']) == '/')?'':dirname($_SERVER['PHP_SELF']);
    }

}