<?php

namespace Marginalia;

##Controller for interfaces (write and design)

class InterfaceCtrl {
    private $db;
    private $logger;
    private $config;

    private $url;
    private $name;
    private $id;
    private $settings;
    private $chapterName;
    public $pads;

    public function __construct(\Monolog\Logger $logger, \Marginalia\MainDB $db, \Marginalia\Config $config, $url){
        $this->logger = $logger;
        $this->db = $db;
        $this->url = $url;
        $this->config = $config;

        $itfRef = $this->db->getInterface($url);
        $this->name = $itfRef['interface_name'];
        $this->id = $itfRef['interface_id'];
        $this->settings = $itfRef['interface_settings'];
        $this->chapterName = $itfRef['chapter_name'];
        $this->pads = $this->loadLinkedPads();

        
        
       
    }


    private function loadLinkedPads(){
        $pads = [];
        $padsRefs = $this->db->getLinkedPads($this->id);

        foreach($padsRefs as $padRef){
            $pads[$padRef['pad_id']] = new PadCtrl($this->logger, $this->config, $padRef, $this);

        }

        return $pads;
    }

    private function buildHTML(){
        $output = '
            <header><h1>Marginalia — '.$this->chapterName.' | '.$this->name.'</h1></header>
            <main class="itf" data-id="'.$this->id.'" data-url="'.$this->url.'">';

        foreach($this->pads as $pad){
            $output .= $pad->buildHTML();
        }
        
        $output .= '</main>';
        return $output;
    }

    private function buildHTMLHeader(){
        $output = '<style>';
        
        foreach($this->pads as $pad){
            $output .= $pad->buildCSS();
        }

        $output .= '</style>';
        return $output;
    }

    public function query($request){
        $response = '';
        switch($request['action']){

            case 'loadInterface':
                $htmlOutput = $this->buildHTML();
                
                $response = ['content' => $htmlOutput];
                break;
            case 'loadHeader':
                $htmlOutput = $this->buildHTMLHeader();
                $response = ['content' => $htmlOutput];
                break;
            case 'insertBlock':
                $fragment = $this->pads[$request['parent']]->insertBlock($request['data'], $request['relation'], $request['n']);
                $response = ['content' => $fragment, 'n' => $request['n'], 'parent' => $request['parent']];
                break;
            case 'editBlock':
                $this->pads[$request['parent']]->editBlock($request['data'], $request['id']);
                break;
            case 'deleteBlock':
                $this->pads[$request['parent']]->deleteBlock($request['id']);
                break;
            case 'sortBlocks':
                $this->pads[$request['parent']]->sortBlocks($request['data']);
                break;
            case 'resizePads':
                foreach($request['data'] as $padRef){
                    
                    $this->pads[$padRef['id']]->changeSetting('width', $padRef['width']);

                    $this->db->saveRowSettings('pads', $padRef['id'], $this->pads[$padRef['id']]->settings);
                }
                break;
            case 'padSetting':
                $this->pads[$request['parent']]->changeSetting($request['data']['key'], $request['data']['value']);
                $this->db->saveRowSettings('pads', $request['parent'], $this->pads[$request['parent']]->settings);

                break;
            

        }
        return $response;
    }
}



class PadCtrl {
    private $db;
    private $logger;
    private $config;
    private $dataPath;
    private $itfCtrl;

    public $id;
    public $settings;
    public $url;
    public $rev;


    public $name;

    public function __construct(\Monolog\Logger $logger, \Marginalia\Config $config, $padRef, $itfCtrl){
        $this->logger = $logger;
        $this->id = $padRef['pad_id'];
        $this->settings = json_decode($padRef['pad_settings']);
        $this->name = $padRef['pad_name'];
        $this->url = $padRef['pad_url'];
        $this->itfCtrl = $itfCtrl;
    
        $this->dataPath = $config->padsDataPath.
            ((substr($config->padsDataPath, -1) != '/')?'/':'').$this->url;
        
        if(!is_dir($this->dataPath)){
            mkdir($this->dataPath);
        }
        $dbPath = $this->dataPath.'/.'.$this->url.'.db';
        $this->db = new PadDB($this->logger, $this->config, $dbPath);
        if(!$this->db->exists()){
            $this->db->create();
            $this->rev = $this->db->insertRev();
        }else{
            $this->db->connect();
            $this->rev = $this->db->getLastRev();

        }

    }

    public function changeSetting($key, $value){
        $this->settings->$key = $value;
        
    }

    public function buildHTMLFragment($fragment, $isLocked = false, $noHighlight = false){
        
        $relation = $fragment['fragment_relation'];
        $classList = 'block';
        $head = '';
        if($relation != null){
            $classList .= ' note';
            $head .= '
                <div class="block-relation relation" data-rel-id="'.$relation.'">
                </div>';
        }

        $output = '
            <div class="'.$classList.'" data-n="'.$fragment['n'].'" data-id="'.$fragment['fragment_id'].'">'
                .$head.
                '<div class="block-main">
                    <div class="block-content">'.$fragment['fragment_content'].'</div>
               
                    <div class="tools">
                        '.((!$isLocked)?'<span class="action button edit" data-parent="'.$this->id.'" data-id="'.$fragment['fragment_id'].'" data-action-type="click" data-action="edit"></span>':'').'
                        '.((!$noHighlight)?'<span class="action button highlight" data-parent="'.$this->id.'" data-id="'.$fragment['fragment_id'].'" data-action-type="click" data-action="highlight"></span>':'').'
                        '.((!$isLocked)?'<span class="action button delete" data-action="delete"  data-action-type="click" data-parent="'.$this->id.'" data-id="'.$fragment['fragment_id'].'"></span>':'').'
                    </div>
                </div>
                <div class="tools">
                    '.((!$isLocked)?'<span class="action button add" data-parent="'.$this->id.'" data-n="'.($fragment['n']+1).'" data-action="add" data-action-type="click">':'').'
                </div>
            </div>'
           ;
        

        return $output;
    }

    public function buildHTML(){
        $nbPads = count($this->itfCtrl->pads);
        $width = (!isset($this->settings->width))?100/$nbPads:$this->settings->width;
        $fontSize = (!isset($this->settings->fontsize))?5:$this->settings->fontsize;
        $nDispMode = (!isset($this->settings->ndisplaymode))?1:$this->settings->ndisplaymode;
        $isLocked = (isset($this->settings->lock) && $this->settings->lock == true)?true:false;
        //check if there is another pad to write to. otherwise, no highlight
        $noHighlight = true;
        foreach($this->itfCtrl->pads as $pad){
            
            if($pad->id == $this->id) continue;
            if(!isset($pad->settings->lock) || $pad->settings->lock == false){
                $noHighlight = false;
                break;
            }

        }

        $output = '
            <section class="pad" style="flex-basis:'.$width.'%" data-lock="'.(($isLocked)?1:0).'" data-id="'.$this->id.'" data-name="'.$this->name.'" data-shortname="'.$this->shortenName($this->name).'" data-url="'.$this->url.'" data-ndisplaymode="'.$nDispMode.'">
                <header class="pad-header">
                    <div class="tools"><span class="action button settings" data-action-type="click" data-parent="'.$this->id.'" data-action="settings"></div>
                    <h2>'.$this->name.'</h2>
                    
                </header>
                <div class="settings">
                        <div>
                            <label>font size: </label><input class="action" data-action-type="change" data-parent="'.$this->id.'" data-action="setFontSize" type="range" min="1" max="10" value="'.$fontSize.'">
                        </div>
                        <div>
                            <label>reference display mode: </label>
                            <select class="action" data-action-type="change" data-parent="'.$this->id.'" data-action="setRefDisplayMode">
                                <option value="1"'.(($nDispMode == 1)?' selected':'').'>pad name a→pad name b:x</option>
                                <option value="2"'.(($nDispMode == 2)?' selected':'').'>pna→pnb:x</option>
                                <option value="3"'.(($nDispMode == 3)?' selected':'').'>x</option>
                            </select>
                        </div>
                        <div>
                            <label>sort notes: </label>
                            <button value="0" class="action" data-action-type="click" data-parent="'.$this->id.'" data-action="sort">by source pad</button> 
                           
                        </div>
                    </div>
                <div class="pad-content" style="font-size:'.(0.5+$fontSize/5).'rem;">
                    <div class="tools">
                    '.((!$isLocked)?'<span class="action button add" data-parent="'.$this->id.'" data-n="1" data-action="add" data-action-type="click"></span>':'').'
                    </div>';
        
        //here go the blocks
        $fragments = $this->db->getFragments($this->rev);
        foreach($fragments as $fragment){
            $output .= $this->buildHTMLFragment($fragment, $isLocked, $noHighlight);
        }

        $output .= '
                </div>
                <div class="action resizer" data-action="resize" data-action-type="mousedown"></div>
            </section>';

        return $output;
    }

    public function buildCSS(){

        $output = '
            .pad[data-id=\''.$this->id.'\'] .selection{
                background-color:hsl('.$this->settings->color.', 90%, 90%);
            }
            .pad[data-id=\''.$this->id.'\'] .pad-header h2::before {
                content: \'\';
                display: inline-block;
                width: 2rem;
                margin-left:-2rem;
                height: 1rem;
                background-color:hsl('.$this->settings->color.', 90%, 90%);
            }
        ';
        foreach($this->itfCtrl->pads as $pad){
            if($pad->id == $this->id) continue;
            
            $output .= '.relation[data-from=\''.$this->id.'\'][data-to=\''.$pad->id.'\']{
                background:linear-gradient(to right, hsl('.$this->settings->color.', 90%, 90%), hsl('.$pad->settings->color.', 90%, 90%));
            }';

        }
        //.relation[data-from=\''.$this->id.'\'] span

        

        return $output;
    }
    public function shortenName($name){
        $out = '';
        $words = explode(' ', trim($name));
        foreach($words as $word){
            $out.=substr($word, 0, 1);
        }
        return $out;
    }
    public function insertBlock($data, $relation, $n){
        
        $fragment = $this->db->insertFragment($data, $relation, $n, $this->rev);
        $isLocked = (isset($this->settings->lock) && $this->settings->lock == true)?true:false;

        if($isLocked)
            return false;
        
        $noHighlight = true;
        foreach($this->itfCtrl->pads as $pad){
            
            if($pad->id == $this->id) continue;
            if(!isset($pad->settings->lock) || $pad->settings->lock == false){
                $noHighlight = false;
                break;
            }
        }

        return $this->buildHTMLFragment($fragment, $isLocked, $noHighlight);
        
    }

    public function editBlock($data, $id){
        return $this->db->updateFragment($data, $id);
    }

    public function deleteBlock($id){
        return $this->db->deleteFragment($id);
    }

    public function sortBlocks($data){
        return $this->db->sortFragments($data);
    }

    public function saveSettings(){
        return $this->db->saveSettings($this->settings);
    }
}