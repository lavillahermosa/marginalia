<?php

$config->dbPath = $config->rootPath.'/data/.marginalia.db';
$config->padsDataPath = $config->rootPath.'/data/pads/';
$config->imgsDataPath = $config->rootPath.'/data/images/';
$config->projectName = 'Marginalia';